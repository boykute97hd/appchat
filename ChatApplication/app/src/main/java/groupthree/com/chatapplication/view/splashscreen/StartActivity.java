package groupthree.com.chatapplication.view.splashscreen;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import groupthree.com.chatapplication.R;
import groupthree.com.chatapplication.presenter.splashscreen.PresenterLogicSplashScreen;
import groupthree.com.chatapplication.view.ActionActivity;
import groupthree.com.chatapplication.view.main.MainActivity;

public class StartActivity extends AppCompatActivity implements ViewSplashScreen{

    private PresenterLogicSplashScreen presenterLogicSplashScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        init();
        presenterLogicSplashScreen.checkCurrenUser();
    }

    private void init() {
        presenterLogicSplashScreen = new PresenterLogicSplashScreen(this);
    }

    @Override
    public void noCurrentUser() {
        Intent intentAction = new Intent(StartActivity.this, ActionActivity.class);
        startActivity(intentAction);
        overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
    }

    @Override
    public void haveCurrenUser() {
        Intent intentAction = new Intent(StartActivity.this, MainActivity.class);
        startActivity(intentAction);
        overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
