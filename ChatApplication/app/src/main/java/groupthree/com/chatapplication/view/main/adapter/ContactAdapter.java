package groupthree.com.chatapplication.view.main.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import groupthree.com.chatapplication.R;
import groupthree.com.chatapplication.model.ContactModel;
import groupthree.com.chatapplication.view.chat.ChatActivity;
import groupthree.com.chatapplication.view.friendprofile.FriendProfileActivity;
import vn.anhnguyen.contextmenu.ContextMenu;
import vn.anhnguyen.contextmenu.IContextMenu;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolder> {
    List<ContactModel> arrFriend;
    int resource;
    Context context;
    public ContactAdapter(List<ContactModel> arrFriend, int resource, Context context) {
        this.arrFriend = arrFriend;
        this.resource = resource;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resource,parent,false);
        ContactAdapter.ViewHolder viewHolder = new ContactAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final ContactModel c = arrFriend.get(position);
        holder.tvName.setText(c.account.displayName);
        if(c.account.avatar.equals("default")){
            holder.imgAvatar.setBackgroundResource(R.drawable.avt);
        }else {
            Picasso.get().load(c.account.avatar).placeholder(R.drawable.avt).into(holder.imgAvatar);
        }
        if(!c.account.status.equals("none")){
            holder.tvStatus.setText(c.account.status);
        }else {
            Date date = new Date(c.date);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
            holder.tvStatus.setText("Bạn bè từ ngày "+simpleDateFormat.format(date));
        }
        if(c.account.online.equals("Online")){
            holder.imgOnline.setVisibility(View.VISIBLE);
        }else {
            holder.imgOnline.setVisibility(View.GONE);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context,ChatActivity.class);
                Activity activity = (Activity) context;
                i.putExtra("acc_id",c.id);
                context.startActivity(i);
                activity.overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                final ArrayList<String> arrMenu = new ArrayList<>();
                arrMenu.add("Trang cá nhân");
                arrMenu.add("Xóa bạn");
                final ContextMenu contextMemu = new ContextMenu(context,arrMenu);
                contextMemu.setOnClick(new IContextMenu() {
                    @Override
                    public void onClickItem(int id) {
                        switch (id){
                            case 0:
                                Intent i = new Intent(context,FriendProfileActivity.class);
                                Activity activity = (Activity) context;
                                i.putExtra("acc_id",c.id);
                                contextMemu.dismiss();

                                context.startActivity(i);
                                activity.overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
                                break;
                            case 1:
                                break;
                        }
                    }
                });
                contextMemu.setName(c.account.displayName);
                contextMemu.show();
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrFriend.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView imgAvatar;
        TextView tvName;
        TextView tvStatus;
        View itemView;
        CircleImageView imgOnline;
        public ViewHolder(View itemView) {
            super(itemView);
            imgAvatar = itemView.findViewById(R.id.imgAvatarSS);
            tvName = itemView.findViewById(R.id.tvName);
            tvStatus = itemView.findViewById(R.id.tvStatus);
            imgOnline = itemView.findViewById(R.id.imgOnline);
            this.itemView = itemView;
        }

    }
}
