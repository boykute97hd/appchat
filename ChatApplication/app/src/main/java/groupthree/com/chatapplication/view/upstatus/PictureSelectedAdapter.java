package groupthree.com.chatapplication.view.upstatus;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.util.ArrayList;

import groupthree.com.chatapplication.R;
import groupthree.com.chatapplication.presenter.upstatus.PresenterLogicUpStatsus;

public class PictureSelectedAdapter extends RecyclerView.Adapter<PictureSelectedAdapter.ViewHolder> {
    private ArrayList<Uri> arrUri;
    private int resource;
    private PresenterLogicUpStatsus presenterLogicUpStatsus;
    public PictureSelectedAdapter(ArrayList<Uri> arrUri, int resource,PresenterLogicUpStatsus presenterLogicUpStatsus) {
        this.arrUri = arrUri;
        this.resource = resource;
        this.presenterLogicUpStatsus = presenterLogicUpStatsus;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resource,parent,false);
        PictureSelectedAdapter.ViewHolder viewHolder = new PictureSelectedAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Uri uri = arrUri.get(position);
        holder.imgSelected.setImageURI(uri);
        holder.imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrUri.remove(position);
                notifyDataSetChanged();
                if(arrUri.size()==0){
                    presenterLogicUpStatsus.setEnableButtonPost(false);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrUri.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imgSelected;
        ImageButton imgClose;
        public ViewHolder(View itemView) {
            super(itemView);
            imgSelected = itemView.findViewById(R.id.imgAnh);
            imgClose = itemView.findViewById(R.id.imgbClose);
        }
    }
}
