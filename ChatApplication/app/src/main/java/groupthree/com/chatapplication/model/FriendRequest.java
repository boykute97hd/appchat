package groupthree.com.chatapplication.model;

public class FriendRequest {
    private String id;
    private Account account;
    public String request_type;
    public long time;

    public FriendRequest() {
    }

    public FriendRequest(String id, String request_type, long time,Account account) {
        this.id = id;
        this.request_type = request_type;
        this.time = time;
        this.account = account;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
