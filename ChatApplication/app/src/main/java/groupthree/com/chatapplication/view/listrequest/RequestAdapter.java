package groupthree.com.chatapplication.view.listrequest;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import groupthree.com.chatapplication.R;
import groupthree.com.chatapplication.model.ContactModel;
import groupthree.com.chatapplication.model.FriendRequest;
import groupthree.com.chatapplication.presenter.listrequest.PresenterLogicRequest;
import groupthree.com.chatapplication.view.friendprofile.FriendProfileActivity;
import groupthree.com.chatapplication.view.main.adapter.ContactAdapter;

public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.ViewHolder>{
    List<FriendRequest> arrRequest;
    int resource;
    PresenterLogicRequest presenterLogicRequest;
    Context context;

    public RequestAdapter(List<FriendRequest> arrRequest, int resource,PresenterLogicRequest presenterLogicRequest, Context context) {
        this.arrRequest = arrRequest;
        this.resource = resource;
        this.presenterLogicRequest = presenterLogicRequest;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView imgAvatar;
        TextView tvName;
        TextView tvStatus;
        Button btnAllow, btnDelince;
        View itemView;
        public ViewHolder(View itemView) {
            super(itemView);
            imgAvatar = itemView.findViewById(R.id.imgAvatarSS);
            tvName = itemView.findViewById(R.id.tvName);
            tvStatus = itemView.findViewById(R.id.tvStatus);
            btnAllow = itemView.findViewById(R.id.btnAllow);
            btnDelince = itemView.findViewById(R.id.btnRemove);
            this.itemView = itemView;
        }
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resource,parent,false);
        RequestAdapter.ViewHolder viewHolder = new RequestAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final FriendRequest friendRequest = arrRequest.get(position);
        holder.tvName.setText(friendRequest.getAccount().displayName);
        if(friendRequest.getAccount().avatar.equals("default")){
            holder.imgAvatar.setBackgroundResource(R.drawable.avt);
        }else {
            Picasso.get().load(friendRequest.getAccount().avatar).placeholder(R.drawable.avt).into(holder.imgAvatar);
        }
        Date date = new Date();
        Date time = new Date(friendRequest.time);
        long tempTime = 0;
        String unit = "";
        if(TimeUnit.MILLISECONDS.toMinutes(date.getTime()-time.getTime())<60){
            tempTime = TimeUnit.MILLISECONDS.toMinutes(date.getTime()-time.getTime());
            unit = "phút";

        }else {
            if(TimeUnit.MICROSECONDS.toHours(date.getTime()-time.getTime())<24){
                tempTime = TimeUnit.MICROSECONDS.toHours(date.getTime()-time.getTime());
                unit = "giờ";
            }else {
                if(TimeUnit.MICROSECONDS.toDays(date.getTime()-time.getTime())<31){
                    tempTime = TimeUnit.MICROSECONDS.toDays(date.getTime()-time.getTime());
                    unit = "ngày";
                }else {
                    tempTime = 0;
                }
            }
        }
        if(tempTime == 0){
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy");
            holder.tvStatus.setText("Từ ngày "+simpleDateFormat.format(time));
        }else {
            holder.tvStatus.setText(tempTime + " " + unit+ " cách đây");
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context,FriendProfileActivity.class);
                i.putExtra("acc_id",friendRequest.getId());
                context.startActivity(i);
                Activity activity = (Activity) context;
                activity.overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);

            }
        });
        holder.btnAllow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenterLogicRequest.allowRequest(friendRequest.getId());
            }
        });
        holder.btnDelince.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenterLogicRequest.delicneRequest(friendRequest.getId());
            }
        });


    }


    @Override
    public int getItemCount() {
        return arrRequest.size();
    }
}
