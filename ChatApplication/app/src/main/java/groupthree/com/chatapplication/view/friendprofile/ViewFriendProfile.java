package groupthree.com.chatapplication.view.friendprofile;

public interface ViewFriendProfile {
    void loadAvatar(String link);
    void getDisplayName(String displayName);
    void getBirthday(String birthday);
    void getGender(String gender);
    void getAnhBia(String link);
    void loadUIFriend(int numUI);
}
