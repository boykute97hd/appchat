package groupthree.com.chatapplication.view.signup;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import groupthree.com.chatapplication.R;
import groupthree.com.chatapplication.presenter.signup.PresenterIpmSignUp;
import groupthree.com.chatapplication.presenter.signup.PresenterLogicSignUp;
import groupthree.com.chatapplication.view.active.ActiveActivity;
import groupthree.com.chatapplication.view.main.MainActivity;

public class SignUpActivity extends AppCompatActivity implements ViewSignUp {
    private ImageButton imgbBack;
    private EditText edtEmail, edtPassword, edtRePassword;
    private Button btnRegister;
    private PresenterLogicSignUp presenterLogicSignUp;
    private Handler handler;
    private Dialog dialogProgress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        //method get control
        init();
        // come back action
        imgbBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.anim_exitintent,R.anim.anim_exit_exit);
            }
        });
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        String tempEmail = edtEmail.getText().toString().trim();
                        String tempPassword = edtPassword.getText().toString().trim();
                        String tempRePassword = edtRePassword.getText().toString().trim();
                        presenterLogicSignUp.register(tempEmail,tempPassword,tempRePassword);
                    }
                },500);
            }
        });
    }


    private void init() {
        //Control
        imgbBack = findViewById(R.id.imgbBack);
        edtEmail = findViewById(R.id.edtEmailRegister);
        edtPassword = findViewById(R.id.edtPasswordRegister);
        edtRePassword = findViewById(R.id.edtRePasswordRegister);
        btnRegister = findViewById(R.id.btnRegister);
        dialogProgress= new Dialog(SignUpActivity.this);
        //Object
        presenterLogicSignUp = new PresenterLogicSignUp(this);
        handler = new Handler();
    }

    @Override
    public void registerSucccess() {
        edtPassword.setText("");
        edtRePassword.setText("");
        Toast.makeText(SignUpActivity.this,"Đăng ký thành công",Toast.LENGTH_SHORT).show();
        Intent intentActive = new Intent(SignUpActivity.this, ActiveActivity.class);
        intentActive.putExtra("back","register");
        startActivity(intentActive);
        overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
    }

    @Override
    public void registerFail(int error) {
        String errorString = "";
        switch (error){
            case 0:
                errorString = "Có lỗi trong quá trình đăng ký!";
                break;
            case 1:
                errorString = "Bạn không được để trống bất kỳ trường nào!";
                break;
            case 2:
                errorString = "Bạn phải nhập đúng dạng Email!";
                break;
            case 3:
                errorString = "Mật khẩu nhập lại không chính xác!";
                break;
            case 4:
                errorString = "Mật khẩu nhập phải gồm 6 kí tự!";
                break;
            case 5:
                errorString = "Email này đã được sử dụng!";
        }
        Toast.makeText(SignUpActivity.this,errorString,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showDialog() {
        dialogProgress.setContentView(R.layout.dialog_progress);
        dialogProgress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogProgress.show();
    }

    @Override
    public void hideDialog() {
        dialogProgress.dismiss();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_exitintent,R.anim.anim_exit_exit);
    }
}
