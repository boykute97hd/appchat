package groupthree.com.chatapplication.view.chat;

import java.util.ArrayList;

import groupthree.com.chatapplication.model.Account;
import groupthree.com.chatapplication.model.MesseageModel;

public interface ViewChat {
    void loadInfo(Account a);
    void loadZoomChat(ArrayList<MesseageModel> arrMessage);
    void hideLayoutPicture();
}
