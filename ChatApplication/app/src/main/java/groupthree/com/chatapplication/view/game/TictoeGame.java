package groupthree.com.chatapplication.view.game;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import groupthree.com.chatapplication.R;

public class TictoeGame extends AppCompatActivity implements View.OnClickListener {

    public Button[][] buttons = new Button[3][3];

    public boolean playerXTurn = true;

    public int roundcount, playerXPoints, playerOPoints;

    public TextView textViewPlayerX, textViewPlayerO;

    public Button btReset, btBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tictoe_game);
        try {
            findControl();
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    String buttonID = "bt_" + i + j;
                    int resID = getResources().getIdentifier(buttonID, "id", getPackageName());
                    buttons[i][j] = findViewById(resID);
                    buttons[i][j].setOnClickListener(this);
                }
            }
            btReset.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    resetGame();
                }
            });

            btBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //resetBoard();
                    finish();
                }
            });
        } catch (Exception ex) {
            Toast.makeText(this, "Error in Main", Toast.LENGTH_SHORT).show();
        }
    }

    private void resetGame() {
        try {
            playerXPoints = 0;
            playerOPoints = 0;
            updatePointText();
            resetBoard();
        } catch (Exception ex) {
            Toast.makeText(this, "Error in resetGame", Toast.LENGTH_SHORT).show();
        }
    }

    public void findControl() {
        textViewPlayerX = findViewById(R.id.tw_pX);
        textViewPlayerO = findViewById(R.id.tw_pO);
        btReset = findViewById(R.id.bt_reset);
        btBack = findViewById(R.id.bt_Back);
    }

    @Override
    public void onClick(View v) {
        try {
            if (!((Button) v).getText().toString().equals("")) {
                return;
            }
            if (playerXTurn) {
                ((Button) v).setText("X");
            } else {
                ((Button) v).setText("O");
            }
            roundcount++;
              if (checkWin()) {
                if (playerXTurn) {
                    playerXWin();
                } else {
                    playerOWin();
                }
            } else if (roundcount == 9) {
                draw();
            } else {
                playerXTurn = !playerXTurn;
            }
        } catch (Exception ex) {
            Toast.makeText(this, "Error in onClickView", Toast.LENGTH_SHORT).show();
        }
    }
    public boolean checkWin() {
        String[][] field = new String[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                field[i][j] = buttons[i][j].getText().toString();
            }
        }

        for (int i = 0; i < 3; i++) {
            if (field[i][0].equals(field[i][1])
                    && field[i][0].equals(field[i][2])
                    && !field[i][0].equals("")) {
                return true;
            }
        }

        for (int i = 0; i < 3; i++) {
            if (field[0][i].equals(field[1][i])
                    && field[0][i].equals(field[2][i])
                    && !field[0][i].equals("")) {
                return true;
            }
        }

        if (field[0][0].equals(field[1][1])
                && field[0][0].equals(field[2][2])
                && !field[0][0].equals("")) {
            return true;
        }

        if (field[0][2].equals(field[1][1])
                && field[0][2].equals(field[2][0])
                && !field[0][2].equals("")) {
            return true;
        }
        return false;
    }
    public void playerXWin() {
        try {
            playerXPoints++;
            Toast.makeText(this, "Player X Wins!", Toast.LENGTH_SHORT).show();
            updatePointText();
            resetBoard();
        } catch (Exception ex) {
            Toast.makeText(this, "Error in PlayerXWin", Toast.LENGTH_SHORT).show();
        }

    }

    public void playerOWin() {
        try {
            playerOPoints++;
            Toast.makeText(this, "Player O Wins!", Toast.LENGTH_SHORT).show();
            updatePointText();
            resetBoard();
        } catch (Exception ex) {
            Toast.makeText(this, "Error in PlayerOWin", Toast.LENGTH_SHORT).show();
        }
    }

    public void draw() {
        try {
            Toast.makeText(this, "Game Draw!", Toast.LENGTH_SHORT).show();
            resetBoard();
        } catch (Exception ex) {
            Toast.makeText(this, "Error in Draw", Toast.LENGTH_SHORT).show();
        }
    }

    public void updatePointText() {
        try {
            textViewPlayerX.setText("Player X: " + playerXPoints);
            textViewPlayerO.setText("Player O: " + playerOPoints);
        } catch (Exception ex) {
            Toast.makeText(this, "Error in updatePoint", Toast.LENGTH_SHORT).show();
        }
    }

    public void resetBoard() {
        try {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    buttons[i][j].setText("");
                }
            }
            roundcount = 0;
            playerXTurn = true;
        } catch (Exception ex) {
            Toast.makeText(this, "Error in resetBoard", Toast.LENGTH_SHORT).show();
        }
    }

}
