package groupthree.com.chatapplication.presenter.main.fragment.messagefragment;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;

import groupthree.com.chatapplication.model.Account;
import groupthree.com.chatapplication.model.Message;
import groupthree.com.chatapplication.view.main.fragement.messeage.ViewMessage;

public class PresenterLogicMessage implements PresenterIpmMessage {
    ViewMessage viewMessage;

    public PresenterLogicMessage(ViewMessage viewMessage) {
        this.viewMessage = viewMessage;
    }

    public ViewMessage getViewMessage() {
        return viewMessage;
    }

    @Override
    public void getDataMeaage() {
        final String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ArrayList<Message> arrMessage = new ArrayList<>();
                DataSnapshot snapshotMess = dataSnapshot.child("Messages").child(uid);
                if(snapshotMess.exists()){
                    for(DataSnapshot snapshot: snapshotMess.getChildren()){
                        Message message = new Message();
                        message.setIdTalk(snapshot.getKey());
                        DataSnapshot snapshotAccount = dataSnapshot.child("Accounts").child(message.getIdTalk());
                        Account account =snapshotAccount.getValue(Account.class);
                        message.setAccount(account);
                        DataSnapshot snapshotLastMess = snapshot.child("last");
                        message.setLastMess(String.valueOf(snapshotLastMess.getValue()));
                        arrMessage.add(message);
                    }
                    getViewMessage().loadData(arrMessage);
                }else {
                    getViewMessage().loadData(arrMessage);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void deleteMessage(String idTalk) {
        final String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("Messages").child(uid);
        databaseReference.child(idTalk).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                getDataMeaage();
            }
        });
    }
}
