package groupthree.com.chatapplication.presenter.signin;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import groupthree.com.chatapplication.view.signin.ViewSignIn;

public class PresenterSignIn implements PresenterIpmSignIn {
    private ViewSignIn viewSignIn;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    public PresenterSignIn(ViewSignIn viewSignIn) {
        this.viewSignIn = viewSignIn;
        mAuth = FirebaseAuth.getInstance();
    }


    @Override
    public void login(String email, String password) {
        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
            getViewSignIn().loginFail(1);
            getViewSignIn().hideDialog();
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            getViewSignIn().loginFail(2);
            getViewSignIn().hideDialog();
        } else {
            mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        getViewSignIn().loginSucccess();
                        getViewSignIn().hideDialog();
                    }else {
                        if(task.getException() instanceof FirebaseAuthInvalidCredentialsException){
                            getViewSignIn().loginFail(3);
                            getViewSignIn().hideDialog();
                        }
                        if(task.getException() instanceof FirebaseAuthInvalidUserException){
                            getViewSignIn().loginFail(4);
                            getViewSignIn().hideDialog();
                        }
                        getViewSignIn().hideDialog();
                        Log.e("Exception", "onComplete: "+task.getException() );
                    }
                }
            });
        }

    }


    public ViewSignIn getViewSignIn() {
        return viewSignIn;
    }

    public void setViewSignIn(ViewSignIn viewSignIn) {
        this.viewSignIn = viewSignIn;
    }
}