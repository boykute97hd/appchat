package groupthree.com.chatapplication.view.main.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import groupthree.com.chatapplication.R;
import groupthree.com.chatapplication.model.ImageModel;
import groupthree.com.chatapplication.view.Image.ImageActivity;

public class PictureStatusAdapter extends RecyclerView.Adapter<PictureStatusAdapter.ViewHolder> {
    private ArrayList<ImageModel> arrImage;
    private Context context;

    public PictureStatusAdapter(ArrayList<ImageModel> arrImage, Context context) {
        this.arrImage = arrImage;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout_pictuerstatus,parent,false);
        PictureStatusAdapter.ViewHolder viewHolder = new PictureStatusAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final ImageModel imageModel = arrImage.get(position);
        Picasso.get().load(imageModel.link).into(holder.img);
        holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context,ImageActivity.class);
                i.putExtra("link",imageModel.link);
                Activity activity = (Activity) context;
                activity.overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrImage.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView img;
        public ViewHolder(View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.imgAnhStatus);
        }
    }
}
