package groupthree.com.chatapplication.view.userprofile;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import groupthree.com.chatapplication.R;
import groupthree.com.chatapplication.presenter.userprofile.PresenterLogicUserProfile;
import groupthree.com.chatapplication.view.Image.ImageActivity;
import groupthree.com.chatapplication.view.changeinfomation.ChangeInformationActivity;

public class UserProfile extends AppCompatActivity implements View.OnClickListener, ViewUserProfile {
    private ImageButton imgbBack;
    private PresenterLogicUserProfile presenterLogicUserProfile;
    private CircleImageView circleImageView;
    private ImageView imgAnhBia;
    private TextView tvNgaySinh, tvGioiTinh, tvTenHienThi, tvSuaThongTinCaNhan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        init();
        presenterLogicUserProfile.loadUserProfile();
        imgbBack.setOnClickListener(this);
        tvSuaThongTinCaNhan.setOnClickListener(this);

    }

    private void init() {
       imgbBack =findViewById(R.id.imgbBack);
       circleImageView = findViewById(R.id.imgAvatar);
       presenterLogicUserProfile = new PresenterLogicUserProfile(this);
       imgAnhBia = findViewById(R.id.imgAnhBia);
       tvNgaySinh = findViewById(R.id.tvNgaySinh);
       tvGioiTinh = findViewById(R.id.tbGioiTinh);
       tvTenHienThi = findViewById(R.id.tvDisplayName);
        tvSuaThongTinCaNhan = findViewById(R.id.tvSuaThongTinCaNhan);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_exitintent,R.anim.anim_exit_exit);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgbBack:
                onBackPressed();
                break;
            case R.id.tvSuaThongTinCaNhan:
                startActivity(new Intent(UserProfile.this, ChangeInformationActivity.class));
                overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
                break;
        }
    }

    @Override
    public void loadAvatar(final String link) {
        if(link.equals("default")){
            circleImageView.setBackgroundResource(R.drawable.avt);
        }else {
            Picasso.get().load(link).into(circleImageView);
            circleImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(UserProfile.this,ImageActivity.class);
                    i.putExtra("link",link);
                    overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
                    startActivity(i);
                }
            });
        }
    }

    @Override
    public void getDisplayName(String displayName) {
        tvTenHienThi.setText(displayName);
    }

    @Override
    public void getBirthday(String birthday) {
        tvNgaySinh.setText("Ngày sinh: "+birthday);
    }

    @Override
    public void getGender(String gender) {
        tvGioiTinh.setText("Giới tính: "+gender);
    }

    @Override
    public void getAnhBia(final String link) {
        if(link.equals("default")){
            imgAnhBia.setBackgroundResource(R.drawable.avt);
        }else {
            Picasso.get().load(link).into(imgAnhBia);
            imgAnhBia.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(UserProfile.this,ImageActivity.class);
                    i.putExtra("link",link);
                    overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
                    startActivity(i);
                }
            });
        }
    }
}
