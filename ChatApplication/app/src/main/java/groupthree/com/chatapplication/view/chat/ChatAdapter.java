package groupthree.com.chatapplication.view.chat;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.view.menu.MenuView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import groupthree.com.chatapplication.R;
import groupthree.com.chatapplication.model.ImageModel;
import groupthree.com.chatapplication.model.MesseageModel;
import groupthree.com.chatapplication.view.Image.ImageActivity;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    private ArrayList<MesseageModel> arrMess;
    private int resource;
    private Context context;

    public ChatAdapter(ArrayList<MesseageModel> arrMess, int resource, Context context) {
        this.arrMess = arrMess;
        this.resource = resource;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
        ChatAdapter.ViewHolder viewHolder = new ChatAdapter.ViewHolder(view);
        return viewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final MesseageModel messeageModel = arrMess.get(position);
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        if (messeageModel.from.equals(uid)) {
            holder.layoutMain.setGravity(Gravity.RIGHT);
            holder.layoutMain2.setGravity(Gravity.RIGHT);
            Drawable drawable = context.getResources().getDrawable(R.drawable.bg_textchat_user);
            holder.layoutChat.setBackground(drawable);
            holder.layoutPictureSingle.setBackground(drawable);
            holder.layoutPictureGroup.setBackground(drawable);
        } else {
            Drawable drawable = context.getResources().getDrawable(R.drawable.bg_textchat_friend);
            holder.layoutMain.setGravity(Gravity.LEFT);
            holder.layoutMain2.setGravity(Gravity.LEFT);
            holder.layoutChat.setBackground(drawable);
            holder.layoutPictureSingle.setBackground(drawable);
            holder.layoutPictureGroup.setBackground(drawable);
        }
        String type;
        if (messeageModel.getArrImage().size() == 0) {
            type = "sms";
        } else if (messeageModel.getArrImage().size() == 1) {
            type = "mms1";
        } else {
            type = "mms2";
        }
        Date now = new Date();
        Date time = new Date(messeageModel.getTime());
        SimpleDateFormat simpleDateFormat;
        if (time.getDay() == now.getDay()) {
            simpleDateFormat = new SimpleDateFormat("HH:mm");
        } else {
            simpleDateFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy");
        }
        String timeString = simpleDateFormat.format(time);
        String mess = messeageModel.getMess();
        switch (type) {
            case "sms":
                String txt="";
                if (mess!=null && mess.contains("0x1F")) {
                    String[] arr = mess.split(" ");
                    for (int i = 0; i < arr.length; i++) {
                        if (arr[i].contains("0x1F")) {
                            int u = Integer.parseInt(arr[i].substring(2), 16);
                            txt = txt + " " + getEmojiByUnicode(u);
                        } else {
                            txt = txt + " " + arr[i];
                        }
                    }
                    holder.tvMess.setText(txt);
                } else {
                    holder.tvMess.setText(mess);
                }
                holder.layoutChat.setVisibility(View.VISIBLE);
                holder.layoutPictureSingle.setVisibility(View.GONE);
                holder.layoutPictureGroup.setVisibility(View.GONE);

                holder.tvTime.setText(timeString);
                break;
            case "mms1":
                holder.layoutChat.setVisibility(View.GONE);
                holder.layoutPictureSingle.setVisibility(View.VISIBLE);
                holder.layoutPictureGroup.setVisibility(View.GONE);
                if (mess.length() == 0) {
                    holder.tvMess1.setVisibility(View.GONE);
                } else {
                    holder.tvMess1.setVisibility(View.VISIBLE);
                    String txt1 = "";
                    if (mess!=null && mess.contains("0x1F")) {
                        String[] arr = mess.split(" ");
                        for (int i = 0; i < arr.length; i++) {
                            if (arr[i].contains("0x1F")) {
                                int u = Integer.parseInt(arr[i].substring(2), 16);
                                txt1 = txt1 + " " + getEmojiByUnicode(u);
                            } else {
                                txt1 = txt1 + " " + arr[i];
                            }
                        }
                        holder.tvMess1.setText(txt1);
                    } else {
                        holder.tvMess1.setText(mess);
                    }
                }
                Picasso.get().load(messeageModel.getArrImage().get(0).link).fit().centerCrop().into(holder.imgAnh);
                holder.imgAnh.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(context, ImageActivity.class);
                        i.putExtra("link", messeageModel.getArrImage().get(0).link);
                        Activity activity = (Activity) context;
                        activity.overridePendingTransition(R.anim.anim_intent, R.anim.anim_exit);
                        context.startActivity(i);
                    }
                });
                holder.tvTime1.setText(timeString);
                break;
            case "mms2":

                holder.layoutChat.setVisibility(View.GONE);
                holder.layoutPictureSingle.setVisibility(View.GONE);
                holder.layoutPictureGroup.setVisibility(View.VISIBLE);
                if (mess.length() == 0) {
                    holder.tvMess2.setVisibility(View.GONE);
                } else {
                    holder.tvMess2.setVisibility(View.VISIBLE);
                    String txt2 = "";
                    if (mess!=null && mess.contains("0x1F")) {
                        String[] arr = mess.split(" ");
                        for (int i = 0; i < arr.length; i++) {
                            if (arr[i].contains("0x1F")) {
                                int u = Integer.parseInt(arr[i].substring(2), 16);
                                txt2 = txt2 + " " + getEmojiByUnicode(u);
                            } else {
                                txt2 = txt2 + " " + arr[i];
                            }
                        }
                        holder.tvMess2.setText(txt2);
                    } else {
                        holder.tvMess2.setText(mess);
                    }
                }
                holder.setArrImage(messeageModel.getArrImage());
                holder.tvTime2.setText(timeString);
                break;
        }

    }

    public String getEmojiByUnicode(int unicode) {
        return new String(Character.toChars(unicode));
    }

    @Override
    public int getItemCount() {
        return arrMess.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout layoutMain, layoutChat, layoutPictureSingle, layoutPictureGroup, layoutMain2;
        TextView tvMess, tvTime, tvMess1, tvTime1, tvMess2, tvTime2;
        ImageView imgAnh;
        RecyclerView rvImage;
        RecyclerView.LayoutManager layoutManager;
        private ArrayList<ImageModel> arrImage;
        private ImageGroupAdapter imageGroupAdapter;
        View itemView;

        public ViewHolder(View itemView) {
            super(itemView);
            tvMess = itemView.findViewById(R.id.tvContentChat);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvMess1 = itemView.findViewById(R.id.tvContentChat1);
            tvTime1 = itemView.findViewById(R.id.tvTime1);
            tvMess2 = itemView.findViewById(R.id.tvContentChat2);
            tvTime2 = itemView.findViewById(R.id.tvTime2);
            layoutMain = itemView.findViewById(R.id.layoutMain);
            layoutChat = itemView.findViewById(R.id.layoutChat);
            layoutPictureSingle = itemView.findViewById(R.id.layoutPictureSingle);
            layoutPictureGroup = itemView.findViewById(R.id.layoutPictureGroup);
            imgAnh = itemView.findViewById(R.id.imgPictureSingle);
            rvImage = itemView.findViewById(R.id.rvImageGroup);
            layoutManager = new GridLayoutManager(itemView.getContext(), 3);
            rvImage.setLayoutManager(layoutManager);
            layoutMain2 = itemView.findViewById(R.id.layout_main2);
            arrImage = new ArrayList<>();
            this.itemView = itemView;
        }

        public void setArrImage(ArrayList<ImageModel> arrImg) {
            this.arrImage.clear();
            this.arrImage.addAll(arrImg);
            if (arrImage.size() % 2 == 0) {
                layoutManager = new GridLayoutManager(itemView.getContext(), 2);
            } else {
                layoutManager = new GridLayoutManager(itemView.getContext(), 3);
            }

            rvImage.setLayoutManager(layoutManager);
            imageGroupAdapter = new ImageGroupAdapter(arrImage, itemView.getContext());
            rvImage.setAdapter(imageGroupAdapter);
        }
    }
}
