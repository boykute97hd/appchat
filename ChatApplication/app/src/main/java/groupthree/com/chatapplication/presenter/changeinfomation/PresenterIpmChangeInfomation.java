package groupthree.com.chatapplication.presenter.changeinfomation;

import android.net.Uri;

public interface PresenterIpmChangeInfomation {
    void updateAvatar(Uri uri);
    void updateWallpaper(Uri uri);
    void showInforOld();
    void updateProfile(String displayName, String birthday);
}
