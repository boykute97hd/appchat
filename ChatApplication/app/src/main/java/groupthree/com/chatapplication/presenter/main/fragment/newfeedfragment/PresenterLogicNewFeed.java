package groupthree.com.chatapplication.presenter.main.fragment.newfeedfragment;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

import groupthree.com.chatapplication.model.Account;
import groupthree.com.chatapplication.model.Comment;
import groupthree.com.chatapplication.model.ContactModel;
import groupthree.com.chatapplication.model.ImageModel;
import groupthree.com.chatapplication.model.Like;
import groupthree.com.chatapplication.model.StatusModel;
import groupthree.com.chatapplication.view.main.fragement.newfeed.ViewNewFeed;

public class PresenterLogicNewFeed implements PresenterIpmNewFeed {
    private ViewNewFeed viewNewFeed;

    public PresenterLogicNewFeed(ViewNewFeed viewNewFeed) {
        this.viewNewFeed = viewNewFeed;
    }

    public ViewNewFeed getViewNewFeed() {
        return viewNewFeed;
    }

    @Override
    public void loadData(final int total) {
        final String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        final DatabaseReference mData = FirebaseDatabase.getInstance().getReference();
        mData.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ArrayList<StatusModel> arrStatusModel = new ArrayList<>();
                DataSnapshot mDataFriend = dataSnapshot.child("Friends").child(uid);
                if(mDataFriend.exists()){
                    for(DataSnapshot snapshot: mDataFriend.getChildren()){
                        DataSnapshot mStatus = dataSnapshot.child("News").child(snapshot.getKey());
                        if(mStatus.exists()){
                            for(DataSnapshot snapshot1:mStatus.getChildren()){
                                StatusModel statusModel = snapshot1.getValue(StatusModel.class);

                                statusModel.setId(snapshot1.getKey());

                                DataSnapshot mAccount = dataSnapshot.child("Accounts").child(snapshot.getKey());
                                Account a = mAccount.getValue(Account.class);
                                a.setId(snapshot.getKey());
                                statusModel.setAccount(a);
                                ArrayList<ImageModel> arrImageModel = new ArrayList<>();
                                if(snapshot1.hasChild("image")){
                                    for(DataSnapshot snapshot2: snapshot1.child("image").getChildren()){
                                        ImageModel imageModel = new ImageModel();
                                        imageModel.link = (String) snapshot2.getValue();
                                        imageModel.setId(snapshot2.getKey());
                                        arrImageModel.add(imageModel);
                                    }
                                }
                                statusModel.setArrImageModel(arrImageModel);
                                ArrayList<Like> arrLike = new ArrayList<>();
                                if(snapshot1.hasChild("like")){
                                    for(DataSnapshot snapshot2: snapshot1.child("like").getChildren()){
                                        Like like = new Like();
                                        String id = snapshot2.getKey();
                                        Account a1 = dataSnapshot.child("Accounts").child(id).getValue(Account.class);
                                        like.setAccount(a1);
                                        like.setTime((Long) snapshot2.getValue());
                                        arrLike.add(like);
                                    }
                                }
                                statusModel.setArrLike(arrLike);
                                ArrayList<Comment> arrComment = new ArrayList<>();
                                if(snapshot1.hasChild("comment")){
                                    for(DataSnapshot snapshot2: snapshot1.child("comment").getChildren()){
                                        Comment comment = new Comment();
                                        String id = (String) snapshot2.child("id").getValue();
                                        Account a1 = dataSnapshot.child("Accounts").child(id).getValue(Account.class);
                                        comment.setAccount(a1);
                                        comment.setTime((Long) snapshot2.child("time").getValue());
                                        comment.setComment((String) snapshot2.child("text").getValue());
                                        String link ="";
                                        if(snapshot2.hasChild("link")){
                                            link = (String) snapshot2.child("link").getValue();
                                        }
                                        comment.setLink(link);
                                        arrComment.add(comment);
                                    }
                                }
                                statusModel.setArrComment(arrComment);
                                arrStatusModel.add(statusModel);
                            }
                        }
                    }

                }
                DataSnapshot mNewsUser = dataSnapshot.child("News").child(uid);
                if(mNewsUser.exists()){
                    for(DataSnapshot snapshot1:mNewsUser.getChildren()){
                        StatusModel statusModel = snapshot1.getValue(StatusModel.class);
                        statusModel.setId(snapshot1.getKey());
                        DataSnapshot mAccount = dataSnapshot.child("Accounts").child(uid);
                        Account a = mAccount.getValue(Account.class);
                        a.setId(uid);
                        statusModel.setAccount(a);
                        ArrayList<ImageModel> arrImageModel = new ArrayList<>();
                        if(snapshot1.hasChild("image")){
                            for(DataSnapshot snapshot2: snapshot1.child("image").getChildren()){
                                ImageModel imageModel = new ImageModel();
                                imageModel.link = (String) snapshot2.getValue();
                                imageModel.setId(snapshot2.getKey());
                                arrImageModel.add(imageModel);
                            }
                        }
                        statusModel.setArrImageModel(arrImageModel);
                        ArrayList<Like> arrLike = new ArrayList<>();
                        if(snapshot1.hasChild("like")){
                            for(DataSnapshot snapshot2: snapshot1.child("like").getChildren()){
                                Like like = new Like();
                                String id = snapshot2.getKey();
                                Account a1 = dataSnapshot.child("Accounts").child(id).getValue(Account.class);
                                like.setAccount(a1);
                                like.setTime((Long) snapshot2.getValue());
                                arrLike.add(like);
                            }
                        }
                        statusModel.setArrLike(arrLike);
                        ArrayList<Comment> arrComment = new ArrayList<>();
                        if(snapshot1.hasChild("comment")){
                            for(DataSnapshot snapshot2: snapshot1.child("comment").getChildren()){
                                Comment comment = new Comment();
                                String id = (String) snapshot2.child("id").getValue();
                                Account a1 = dataSnapshot.child("Accounts").child(id).getValue(Account.class);
                                comment.setAccount(a1);
                                comment.setTime((Long) snapshot2.child("time").getValue());
                                comment.setComment((String) snapshot2.child("text").getValue());
                                String link ="";
                                if(snapshot2.hasChild("link")){
                                    link = (String) snapshot2.child("link").getValue();
                                }
                                comment.setLink(link);
                                arrComment.add(comment);
                            }
                        }
                        statusModel.setArrComment(arrComment);
                        arrStatusModel.add(statusModel);
                    }
                }
                Collections.sort(arrStatusModel, new Comparator<StatusModel>() {
                    @Override
                    public int compare(StatusModel o1, StatusModel o2) {
                        return (int) (o2.time-o1.time);
                    }
                });
                if(arrStatusModel.size()<total){
                    getViewNewFeed().loadData(arrStatusModel);
                }else {
                    ArrayList<StatusModel> arrSend = new ArrayList<>();
                    for (int i = 0; i < total; i++) {
                        arrSend.add(arrStatusModel.get(i));
                    }
                    getViewNewFeed().loadData(arrSend);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void like(String uid,String idPost) {
        DatabaseReference mPost = FirebaseDatabase.getInstance().getReference().child("News").child(uid).child(idPost).child("like");
        String cid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        mPost.child(cid).setValue(System.currentTimeMillis());
    }

    @Override
    public void unLike(String uid, String idPost) {
        DatabaseReference mPost = FirebaseDatabase.getInstance().getReference().child("News").child(uid).child(idPost).child("like");
        String cid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        mPost.child(cid).removeValue();
    }
}
