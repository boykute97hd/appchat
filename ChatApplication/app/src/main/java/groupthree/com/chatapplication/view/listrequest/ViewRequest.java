package groupthree.com.chatapplication.view.listrequest;

import java.util.ArrayList;

import groupthree.com.chatapplication.model.FriendRequest;

public interface ViewRequest {
    void loadData(ArrayList<FriendRequest> temp);
}
