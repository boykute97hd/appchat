package groupthree.com.chatapplication.presenter.upstatus;

import android.net.Uri;

import java.util.ArrayList;

public interface PresenterIpmUpStatus {
    void loadAccount();
    void setEnableButtonPost(boolean enable);
    void upStatus(String text, ArrayList<Uri> arrUri);
}
