package groupthree.com.chatapplication.view.searchfriend;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import groupthree.com.chatapplication.R;
import groupthree.com.chatapplication.model.Account;
import groupthree.com.chatapplication.view.friendprofile.FriendProfileActivity;
import groupthree.com.chatapplication.view.userprofile.UserProfile;

public class AdapterRecyclerViewFriend extends RecyclerView.Adapter<AdapterRecyclerViewFriend.ViewHolder> {
    private List<Account> arrAccount;
    private int resource;
    private Context context;
    public List<Account> getArrAccount() {
        return arrAccount;
    }

    public AdapterRecyclerViewFriend(Context context,List<Account> arrAccount, int resource) {
        this.arrAccount = arrAccount;
        this.resource = resource;
        this.context = context;
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        CircleImageView imgAvatar;
        TextView tvName;
        Context context;
        View itemView;
        private List<Account> accounts;
        public ViewHolder(View itemView,Context context, List<Account> accounts) {
            super(itemView);
            imgAvatar = itemView.findViewById(R.id.imgAvatarSS);
            tvName = itemView.findViewById(R.id.tvName);
            this.context = context;
            this.accounts = accounts;
            this.itemView = itemView;
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resource,parent,false);
        ViewHolder viewHolder = new ViewHolder(view,context,arrAccount);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final Account a = arrAccount.get(position);
        holder.tvName.setText(a.displayName);
        if(a.avatar.equals("default")){
            holder.imgAvatar.setBackgroundResource(R.drawable.avt);
        }else {
            Picasso.get().load(a.avatar).networkPolicy(NetworkPolicy.OFFLINE).placeholder(R.drawable.avt).into(holder.imgAvatar, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {
                    Picasso.get().load(a.avatar).placeholder(R.drawable.avt).into(holder.imgAvatar);
                }
            });
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i;
                if(a.getId().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())){
                    i =new Intent(context,UserProfile.class);
                }else {
                    i = new Intent(context,FriendProfileActivity.class);
                    i.putExtra("acc_id",a.getId());
                }
                context.startActivity(i);
                Activity activity = (Activity) context;
                activity.overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrAccount.size();
    }
}
