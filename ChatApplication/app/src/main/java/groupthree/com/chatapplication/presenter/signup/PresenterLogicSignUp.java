package groupthree.com.chatapplication.presenter.signup;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


import java.util.HashMap;

import groupthree.com.chatapplication.view.signup.ViewSignUp;

public class PresenterLogicSignUp implements PresenterIpmSignUp{
    private ViewSignUp viewSignUp;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    public PresenterLogicSignUp(ViewSignUp viewSignUp) {
        this.viewSignUp = viewSignUp;
        mAuth = FirebaseAuth.getInstance();
    }

    public ViewSignUp getViewSignUp() {
        return viewSignUp;
    }

    public void setViewSignUp(ViewSignUp viewSignUp) {
        this.viewSignUp = viewSignUp;
    }

    @Override
    public void register(String email, String password, String rePassword) {
        if(TextUtils.isEmpty(email) || TextUtils.isEmpty(password) || TextUtils.isEmpty(rePassword)){
            // Empty form
            getViewSignUp().registerFail(1);
            getViewSignUp().hideDialog();
        }else if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            //Regular Email
            getViewSignUp().registerFail(2);
            getViewSignUp().hideDialog();
        }else if(!password.equals(rePassword)){
            //Password not equal Re-Password
            getViewSignUp().registerFail(3);
            getViewSignUp().hideDialog();
        }else if(password.length()<6) {
            // Password weak
            getViewSignUp().registerFail(4);
            getViewSignUp().hideDialog();
        }else{
            mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
               if(task.isSuccessful()){
                   //Get Current User
                   FirebaseUser currentUsser = mAuth.getCurrentUser();
                    //Send Email Verify
                   currentUsser.sendEmailVerification();
                   //Set property [active, first login] for user
                   String uid = currentUsser.getUid();
                   mDatabase = FirebaseDatabase.getInstance().getReference().child("Accounts").child(uid);
                   HashMap<String,String> map = new HashMap<>();
                   map.put("firstLogin","true");
                   map.put("active","false");
                   map.put("displayName","Người sử dụng");
                   map.put("birthday","01/01/1990");
                   map.put("gender","Nam");
                   map.put("wallpaper","default");
                   map.put("avatar","default");
                   map.put("status","none");
                   mDatabase.setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                       @Override
                       public void onComplete(@NonNull Task<Void> task) {
                           if(task.isSuccessful()){
                               getViewSignUp().registerSucccess();
                               getViewSignUp().hideDialog();
                           }else {
                               getViewSignUp().registerFail(0);
                               getViewSignUp().hideDialog();
                           }
                       }
                   });
               }else {
                   if(task.getException() instanceof FirebaseAuthUserCollisionException){
                       //Email is Registed
                       getViewSignUp().registerFail(5);
                       getViewSignUp().hideDialog();
                   }else {
                       //Very Error
                       getViewSignUp().registerFail(0);
                       getViewSignUp().hideDialog();
                   }
               }
                }
            });
        }
    }
}
