package groupthree.com.chatapplication.model;

public class Comment {
    private Account account;
    private String comment;
    private long time;
    private String link;

    public Comment() {
    }

    public Comment(Account account, String comment, long time, String link) {
        this.account = account;
        this.comment = comment;
        this.time = time;
        this.link = link;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
