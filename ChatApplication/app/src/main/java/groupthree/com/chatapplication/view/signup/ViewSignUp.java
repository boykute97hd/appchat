package groupthree.com.chatapplication.view.signup;

public interface ViewSignUp {
    void registerSucccess();
    void registerFail(int error);
    void showDialog();
    void hideDialog();
}
