package groupthree.com.chatapplication.view.friendprofile;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import groupthree.com.chatapplication.R;
import groupthree.com.chatapplication.presenter.friendproflie.PresenterLogicFriendProfile;
import groupthree.com.chatapplication.view.Image.ImageActivity;
import groupthree.com.chatapplication.view.chat.ChatActivity;
import groupthree.com.chatapplication.view.userprofile.UserProfile;

public class FriendProfileActivity extends AppCompatActivity implements View.OnClickListener,ViewFriendProfile {
    //Controll
    private ImageButton imgBack;
    private ImageView imgAnhBia;
    private CircleImageView imgAvatar;
    private TextView tbDisplayName, tvNgaySinh, tvGender,tvDisplayName;
    private LinearLayout linearBar, linearAdd;
    private Button btnAddFriend, btnMesseage, btnAllow, btnRemove;
    //Presenter
    private PresenterLogicFriendProfile presenterLogicFriendProfile;
    private String uidTemp = null;
    private int loadUI = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_profile);
        init();

        //su kien click
        imgBack.setOnClickListener(this);
        btnAddFriend.setOnClickListener(this);
        btnAllow.setOnClickListener(this);
        btnRemove.setOnClickListener(this);
        btnMesseage.setOnClickListener(this);
        //end su kien click

        //Get ID Account
        uidTemp =  getIntent().getStringExtra("acc_id");
        //LoadData
        presenterLogicFriendProfile.loadFriendProfile(uidTemp);
        presenterLogicFriendProfile.loadUI(uidTemp);

    }
    //
    //Phuong Thuc nay dung de ánh xạ các ViewControll
    //
    private void init() {
        imgBack = findViewById(R.id.imgbBack);
        imgAnhBia = findViewById(R.id.imgAnhBia);
        imgAvatar = findViewById(R.id.imgAvatar);
        tbDisplayName = findViewById(R.id.tbDisplayName);
        linearBar = findViewById(R.id.linerBar);
        linearAdd = findViewById(R.id.linearAdd);
        btnAddFriend = findViewById(R.id.btnAddFriend);
        btnMesseage = findViewById(R.id.btnMesseage);
        btnAllow = findViewById(R.id.btnAllow);
        btnRemove = findViewById(R.id.btnRemove);
        tvNgaySinh = findViewById(R.id.tvNgaySinh);
        tvGender = findViewById(R.id.tbGioiTinh);
        tvDisplayName = findViewById(R.id.tvDisplayName);
        presenterLogicFriendProfile = new PresenterLogicFriendProfile(this);
    }

    //
    //Phuong Thuc Nay dung de Tro ve
    //
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_exitintent,R.anim.anim_exit_exit);
    }
    //
    //Phuong thuc su kien onClick cho các Button
    //
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgbBack:
                onBackPressed();
                break;
            case R.id.btnAllow:
                eventClickAllow();
                break;
            case R.id.btnRemove:
                eventClickRemove();
                break;
            case R.id.btnMesseage:
                eventClickMesseage();
                break;
            case R.id.btnAddFriend:
                eventClickAddFriend();
                break;
        }
    }

    //
    //Sự kiện nút thêm bạn bè
    //
    private void eventClickAddFriend() {
        if(loadUI == 0){
            presenterLogicFriendProfile.sendRequestFriend(uidTemp);
            loadUIFriend(2);
            Toast.makeText(FriendProfileActivity.this,"Gửi lời mời kết bạn thành công!",Toast.LENGTH_SHORT).show();
        }else if(loadUI ==2 ) {
            AlertDialog.Builder builder = new AlertDialog.Builder(FriendProfileActivity.this,R.style.AlertDialogCustom);
            builder.setMessage("Bạn muốn xóa lời mời kết bạn tới người ngày ?");
            builder.setNegativeButton("Có", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    presenterLogicFriendProfile.removeRequestFriend(uidTemp);
                    Toast.makeText(FriendProfileActivity.this,"Xóa lời mời kết bạn thành công!",Toast.LENGTH_SHORT).show();
                }
            });
            builder.setPositiveButton("Không", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.create().show();
        }else if(loadUI == 3){
            AlertDialog.Builder builder = new AlertDialog.Builder(FriendProfileActivity.this,R.style.AlertDialogCustom);
            builder.setMessage("Bạn muốn hủy kết bạn với người ngày ?");
            builder.setNegativeButton("Có", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    presenterLogicFriendProfile.removeFriends(uidTemp);
                    Toast.makeText(FriendProfileActivity.this,"Hủy kết bạn thành công!",Toast.LENGTH_SHORT).show();
                }
            });
            builder.setPositiveButton("Không", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.create().show();
        }
    }

    //
    //Sự kiện click nút Messeage - Intent Sang Trang Nhắn Tin vs Bạn bè
    //
    private void eventClickMesseage() {
        Intent i = new Intent(FriendProfileActivity.this,ChatActivity.class);
        i.putExtra("acc_id",uidTemp);
        startActivity(i);
        overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);

    }

    //
    //Sự kiện click nút Remove Lời mời
    //
    private void eventClickRemove() {
        presenterLogicFriendProfile.declineRequest(uidTemp);
    }
    //
    //Sự kiện click nút Allow Lời mời
    //
    private void eventClickAllow() {
        presenterLogicFriendProfile.allowRequest(uidTemp);
    }

    @Override
    public void loadAvatar(final String link) {
        if(link.equals("default")){
            imgAvatar.setBackgroundResource(R.drawable.avt);
        }else {
            Picasso.get().load(link).networkPolicy(NetworkPolicy.OFFLINE).placeholder(R.drawable.avt).into(imgAvatar, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {
                    Picasso.get().load(link).placeholder(R.drawable.avt).into(imgAvatar);
                }
            });
            imgAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(FriendProfileActivity.this,ImageActivity.class);
                    i.putExtra("link",link);
                    overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
                    startActivity(i);
                }
            });
        }

    }

    @Override
    public void getDisplayName(String displayName) {
        tbDisplayName.setText(displayName);
        tvDisplayName.setText(displayName);
    }

    @Override
    public void getBirthday(String birthday) {
        tvNgaySinh.setText("Ngày sinh: "+birthday);
    }

    @Override
    public void getGender(String gender) {
        tvGender.setText("Giới tính: "+gender);
    }

    @Override
    public void getAnhBia(final String link) {
        if(link.equals("default")){
            imgAnhBia.setBackgroundResource(R.drawable.avt);
        }else {
            Picasso.get().load(link).networkPolicy(NetworkPolicy.OFFLINE).placeholder(R.drawable.anhbia).into(imgAnhBia, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {
                    Picasso.get().load(link).placeholder(R.drawable.anhbia).into(imgAnhBia);
                }
            });
            imgAnhBia.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(FriendProfileActivity.this,ImageActivity.class);
                    i.putExtra("link",link);
                    overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
                    startActivity(i);
                }
            });
        }

    }

    @Override
    public void loadUIFriend(int numUI) {
        loadUI = numUI;
        if(numUI == 0){
            Drawable top = getResources().getDrawable(R.drawable.add_user);
            btnAddFriend.setCompoundDrawablesWithIntrinsicBounds(null,top,null,null);
            btnAddFriend.setText("Thêm bạn bè");
            btnAddFriend.setEnabled(true);
            linearAdd.setVisibility(View.GONE);
        }
        if(numUI == 1){
            Drawable top = getResources().getDrawable(R.drawable.friend);
            btnAddFriend.setCompoundDrawablesWithIntrinsicBounds(null,top,null,null);
            btnAddFriend.setText("Trả lời");
            btnAddFriend.setEnabled(false);
            linearAdd.setVisibility(View.VISIBLE);
        }
        if(numUI == 2){
            Drawable top = getResources().getDrawable(R.drawable.friend);
            btnAddFriend.setCompoundDrawablesWithIntrinsicBounds(null,top,null,null);
            btnAddFriend.setText("Đã gửi lời mời");
            btnAddFriend.setEnabled(true);
            linearAdd.setVisibility(View.GONE);
        }
        if(numUI == 3){
            Drawable top = getResources().getDrawable(R.drawable.friend);
            btnAddFriend.setCompoundDrawablesWithIntrinsicBounds(null,top,null,null);
            btnAddFriend.setText("Bạn bè");
            btnAddFriend.setEnabled(true);
            linearAdd.setVisibility(View.GONE);
        }
    }


}
