package groupthree.com.chatapplication.view.main.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import groupthree.com.chatapplication.R;
import groupthree.com.chatapplication.model.Message;
import groupthree.com.chatapplication.presenter.main.fragment.messagefragment.PresenterLogicMessage;
import groupthree.com.chatapplication.util.ContextMenu.ContextMenu;
import groupthree.com.chatapplication.util.ContextMenu.IContextMenu;
import groupthree.com.chatapplication.view.chat.ChatActivity;
import groupthree.com.chatapplication.view.friendprofile.FriendProfileActivity;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder>{
    private ArrayList<Message> arrMessage;
    private Context context;
    private PresenterLogicMessage presenterLogicMessage;
    public MessageAdapter(ArrayList<Message> arrMessage, Context context, PresenterLogicMessage presenterLogicMessage) {
        this.arrMessage = arrMessage;
        this.context = context;
        this.presenterLogicMessage = presenterLogicMessage;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_message,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }
    public String getEmojiByUnicode(int unicode) {
        return new String(Character.toChars(unicode));
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Message message = arrMessage.get(position);
        if(message.getAccount().avatar.equals("default")){
            holder.imgAvt.setBackgroundResource(R.drawable.avt);
        }else {
            Picasso.get().load(message.getAccount().avatar).placeholder(R.drawable.avt).into(holder.imgAvt);
        }

        holder.tvName.setText(message.getAccount().displayName);
        String txt="";
        if (message.getLastMess()!=null && message.getLastMess().contains("0x1F")) {
            String[] arr = message.getLastMess().split(" ");
            for (int i = 0; i < arr.length; i++) {
                if (arr[i].contains("0x1F")) {
                    int u = Integer.parseInt(arr[i].substring(2), 16);
                    txt = txt + " " + getEmojiByUnicode(u);
                } else {
                    txt = txt + " " + arr[i];
                }
            }
            holder.tvMess.setText(txt);
        } else {
            holder.tvMess.setText(message.getLastMess());
        }
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context,ChatActivity.class);
                Activity activity = (Activity) context;
                i.putExtra("acc_id",message.getIdTalk());
                context.startActivity(i);
                activity.overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
            }
        });
        holder.view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                final ArrayList<String> arrMenu = new ArrayList<>();
                arrMenu.add("Trang cá nhân");
                arrMenu.add("Xóa cuộc trò chuyện");
                final ContextMenu contextMemu = new ContextMenu(context,arrMenu);
                contextMemu.setOnClick(new IContextMenu() {
                    @Override
                    public void onClickItem(int id) {
                        switch (id){
                            case 0:
                                Intent i = new Intent(context,FriendProfileActivity.class);
                                Activity activity = (Activity) context;
                                i.putExtra("acc_id",message.getIdTalk());
                                contextMemu.dismiss();

                                context.startActivity(i);
                                activity.overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
                                break;
                            case 1:
                                AlertDialog.Builder builder =new AlertDialog.Builder(context,R.style.AlertDialogCustom);
                                builder.setMessage("Bạn muốn xóa toàn bộ cuộc trò chuyện này ?");
                                builder.setPositiveButton("Có", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        presenterLogicMessage.deleteMessage(message.getIdTalk());
                                    }
                                });
                                builder.setNegativeButton("Không", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                contextMemu.dismiss();
                                builder.create().show();
                                break;
                        }
                    }
                });
                contextMemu.setName(message.getAccount().displayName);
                contextMemu.show();
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrMessage.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvMess;
        CircleImageView imgAvt;
        View view;
        public ViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            tvMess = itemView.findViewById(R.id.tvLassmees);
            tvName = itemView.findViewById(R.id.tvName);
            imgAvt = itemView.findViewById(R.id.imgAvatarSS);
        }
    }
}
