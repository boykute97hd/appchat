package groupthree.com.chatapplication.model;

public class Message {
    private String idTalk;
    private Account account;
    private String lastMess;
    public Message() {
    }

    public Message(String idTalk, Account account, String lastMess) {
        this.idTalk = idTalk;
        this.account = account;
        this.lastMess = lastMess;
    }

    public String getIdTalk() {
        return idTalk;
    }

    public void setIdTalk(String idTalk) {
        this.idTalk = idTalk;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getLastMess() {
        return lastMess;
    }

    public void setLastMess(String lastMess) {
        this.lastMess = lastMess;
    }
}
