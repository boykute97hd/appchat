package groupthree.com.chatapplication.view.main.fragement.other;

public interface ViewOther {
    void getDisplayName(String displayName);
    void getAvatar(String avatar);
}
