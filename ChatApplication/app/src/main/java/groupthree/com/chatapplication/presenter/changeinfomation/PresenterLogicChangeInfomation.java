package groupthree.com.chatapplication.presenter.changeinfomation;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.HashMap;
import java.util.Map;

import groupthree.com.chatapplication.model.Account;
import groupthree.com.chatapplication.view.changeinfomation.ViewChangeInfomation;

public class PresenterLogicChangeInfomation implements PresenterIpmChangeInfomation {
    private ViewChangeInfomation viewChangeInfomation;
    FirebaseUser mUser;
    FirebaseDatabase mData;

    public PresenterLogicChangeInfomation(ViewChangeInfomation viewChangeInfomation) {
        this.viewChangeInfomation = viewChangeInfomation;
        mUser = FirebaseAuth.getInstance().getCurrentUser();
        mData = FirebaseDatabase.getInstance();
    }

    public ViewChangeInfomation getViewChangeInfomation() {
        return viewChangeInfomation;
    }
    //Cập nhật ảnh đại diện
    @Override
    public void updateAvatar(Uri uri) {
        final String uid = mUser.getUid();
        final DatabaseReference mDataAlbum = mData.getReference().child("Albums").child(uid).child("avt");
        final DatabaseReference mReference = mData.getReference();
        final String id = mDataAlbum.push().getKey();
        final StorageReference storageUser = FirebaseStorage.getInstance().getReference().child("image").child(uid).child(id+".jpg");
        storageUser.putFile(uri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if(task.isSuccessful()){
                    storageUser.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            final String downloadUrl = uri.toString();
                            final Map update_hashMap = new HashMap<>();
                            update_hashMap.put("/Accounts/"+uid+"/avatar",downloadUrl);
                            update_hashMap.put("/Albums/"+uid+"/avt/"+id,downloadUrl);
                            mReference.updateChildren(update_hashMap).addOnCompleteListener(new OnCompleteListener() {
                                @Override
                                public void onComplete(@NonNull Task task) {
                                    if(task.isSuccessful()){
                                        getViewChangeInfomation().updateAvatarSucccess();
                                        getViewChangeInfomation().hideDialog();
                                    }else {
                                        getViewChangeInfomation().updateAvatarFail("Có lỗi");
                                        getViewChangeInfomation().hideDialog();
                                    }
                                }
                            });
                        }
                    });
                }
            }
        });
    }
    //Update Ảnh bìa
    @Override
    public void updateWallpaper(Uri uri) {
        final String uid = mUser.getUid();
        final DatabaseReference mDataAlbum = mData.getReference().child("Albums").child(uid).child("wallpaper");
        final DatabaseReference mReference = mData.getReference();
        final String id = mDataAlbum.push().getKey();
        final StorageReference storageUser = FirebaseStorage.getInstance().getReference().child("image").child(uid).child(id+".jpg");
        storageUser.putFile(uri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if(task.isSuccessful()){
                    storageUser.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            final String downloadUrl = uri.toString();
                            final Map update_hashMap = new HashMap<>();
                            update_hashMap.put("/Accounts/"+uid+"/wallpaper",downloadUrl);
                            update_hashMap.put("/Albums/"+uid+"/wallpaper/"+id,downloadUrl);
                            mReference.updateChildren(update_hashMap).addOnCompleteListener(new OnCompleteListener() {
                                @Override
                                public void onComplete(@NonNull Task task) {
                                    if(task.isSuccessful()){
                                        getViewChangeInfomation().updateWallpaperSuccsess();
                                        getViewChangeInfomation().hideDialog();
                                    }else {
                                        getViewChangeInfomation().updateWallpaperFail("Có lỗi");
                                        getViewChangeInfomation().hideDialog();
                                    }
                                }
                            });
                        }
                    });
                }
            }
        });
    }
    //Hiển thị thông tin cũ
    @Override
    public void showInforOld() {
        final DatabaseReference mDataAccount = mData.getReference().child("Accounts").child(mUser.getUid());
        mDataAccount.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Account account = dataSnapshot.getValue(Account.class);
                getViewChangeInfomation().showInforOld(account);
                Log.d("Acc", "onDataChange: "+account.displayName);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    //Cập nhật thông tin mới
    @Override
    public void updateProfile(String displayName, String birthday) {
        final HashMap<String,Object> map = new HashMap<>();
        map.put("displayName",displayName);
        map.put("birthday",birthday);
        DatabaseReference mDataAccount = mData.getReference().child("Accounts").child(mUser.getUid());
        mDataAccount.updateChildren(map).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    getViewChangeInfomation().updateProfileSucccess();
                    getViewChangeInfomation().hideDialog();
                }else {
                    getViewChangeInfomation().updateProfileFai("Có lỗi");
                    getViewChangeInfomation().hideDialog();
                }
            }
        });
    }
}
