package groupthree.com.chatapplication.view.changeinfomation;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import groupthree.com.chatapplication.R;
import groupthree.com.chatapplication.model.Account;
import groupthree.com.chatapplication.presenter.changeinfomation.PresenterLogicChangeInfomation;
import groupthree.com.chatapplication.view.firstlogin.SetInfoFirstLogin;

public class ChangeInformationActivity extends AppCompatActivity implements View.OnClickListener, ViewChangeInfomation{
    private ImageButton imgbBack;
    private ImageView imgAnhDaiDien,imgAnhBia;
    private Dialog dialogProgress;
    private Button btnChangeAvt, btnChangeWallpaper, btnChangeProfile;
    private EditText edtDisplayName, edtBirhday;
    private static final int GALLERY_PICK_AVATAR = 1;
    private static final int GALLERY_PICK_WALL_PAPER = 2;
    private PresenterLogicChangeInfomation presenterLogicChangeInfomation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chang_infomation);
        init();
        presenterLogicChangeInfomation.showInforOld();
        imgbBack.setOnClickListener(this);
        btnChangeAvt.setOnClickListener(this);
        btnChangeWallpaper.setOnClickListener(this);
        btnChangeProfile.setOnClickListener(this);
    }

    private void init() {
        imgbBack = findViewById(R.id.imgbBack);
        dialogProgress = new Dialog(ChangeInformationActivity.this);
        btnChangeAvt = findViewById(R.id.btnChangeAvt);
        btnChangeWallpaper = findViewById(R.id.btnChangeWallpaper);
        presenterLogicChangeInfomation = new PresenterLogicChangeInfomation(this);
        imgAnhDaiDien = findViewById(R.id.imgAnhDaiDien);
        imgAnhBia = findViewById(R.id.imgAnhBia);
        edtDisplayName = findViewById(R.id.edtDisplayName);
        edtBirhday = findViewById(R.id.edtNgaySinh);
        btnChangeProfile = findViewById(R.id.btnChangeProfile);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgbBack:
                onBackPressed();
                break;
            case R.id.btnChangeAvt:
                intentGallery(GALLERY_PICK_AVATAR);
                break;
            case R.id.btnChangeWallpaper:
                intentGallery(GALLERY_PICK_WALL_PAPER);
                break;
            case R.id.edtNgaySinh:
                pickDate();
                break;
            case R.id.btnChangeProfile:
                showDialog();
                presenterLogicChangeInfomation.updateProfile(edtDisplayName.getText().toString(),edtBirhday.getText().toString());
                break;
        }
    }

    private void pickDate() {
        String birthTemp = edtBirhday.getText().toString().trim();
        // Progress Date String
        String temp[] = birthTemp.split("/");
        int y = Integer.parseInt(temp[2]);
        int m = Integer.parseInt(temp[1]);
        int d = Integer.parseInt(temp[0]);
        final Calendar calendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(ChangeInformationActivity.this,R.style.DateTimePicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(year,month,dayOfMonth);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                edtBirhday.setText(simpleDateFormat.format(calendar.getTime()));
            }
        },y,m-1,d);
        datePickerDialog.show();
    }

    private void intentGallery(int option) {
        Intent iGallery = new Intent();
        iGallery.setType("image/*");
        iGallery.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(iGallery,"Chọn ảnh"),option);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1 && resultCode == RESULT_OK){
            showDialog();
            Uri avatarUri = data.getData();
            presenterLogicChangeInfomation.updateAvatar(avatarUri);
        }else if(requestCode == GALLERY_PICK_WALL_PAPER && resultCode == RESULT_OK){
            showDialog();
            Uri wallpaperUri = data.getData();
            presenterLogicChangeInfomation.updateWallpaper(wallpaperUri);
        }
    }


    @Override
    public void updateAvatarSucccess() {
        Toast.makeText(ChangeInformationActivity.this,"Đổi ảnh đại diện thành công",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void updateAvatarFail(String message) {
        Toast.makeText(ChangeInformationActivity.this,message,Toast.LENGTH_SHORT).show();

    }

    @Override
    public void updateWallpaperSuccsess() {
        Toast.makeText(ChangeInformationActivity.this,"Đổi ảnh bìa thành công",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void updateWallpaperFail(String message) {
        Toast.makeText(ChangeInformationActivity.this,message,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void updateProfileSucccess() {
        Toast.makeText(ChangeInformationActivity.this,"Đổi thông tin thành công",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void updateProfileFai(String message) {
        Toast.makeText(ChangeInformationActivity.this,message,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showInforOld(Account account) {
        if(account.avatar.equals("default")){
            imgAnhDaiDien.setImageResource(R.drawable.avt);
        }else {
            Picasso.get().load(account.avatar).placeholder(R.drawable.avt).into(imgAnhDaiDien);
        }
        if(account.wallpaper.equals("default")){
            imgAnhBia.setImageResource(R.drawable.anhbia);
        }else {
            Picasso.get().load(account.wallpaper).into(imgAnhBia);
        }
        edtBirhday.setText(account.birthday);
        edtDisplayName.setText(account.displayName);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_exitintent,R.anim.anim_exit_exit);
    }

    @Override
    public void showDialog() {
        dialogProgress.setContentView(R.layout.dialog_progress);
        dialogProgress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogProgress.setCanceledOnTouchOutside(false);
        dialogProgress.show();
    }

    @Override
    public void hideDialog() {
        dialogProgress.dismiss();
    }
}
