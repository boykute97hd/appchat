package groupthree.com.chatapplication.view.game;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.Random;

import groupthree.com.chatapplication.R;

public class evenodddice extends AppCompatActivity {
    Button b_roll, b_even, b_odd, b_back;
    TextView tv_cpu, tv_you, tv_status, tv_sum;
    ImageView iv_dice1, iv_dice2;
    RadioGroup rg_change;
    RadioButton rb_def, rb_bw;
    int cpuPoint = 0, youPoint = 0;
    String currentPick = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evenodddice);
        mainApp();
    }

    public void mainApp() {
        b_roll = findViewById(R.id.b_roll);
        b_even = findViewById(R.id.b_even);
        b_odd = findViewById(R.id.b_odd);
        b_back = findViewById(R.id.b_back);

        rg_change = findViewById(R.id.rg_change);
        rb_def = findViewById(R.id.rb_def);
        rb_bw = findViewById(R.id.rb_col);


        tv_cpu = findViewById(R.id.tv_cpu);
        tv_you = findViewById(R.id.tv_you);
        tv_status = findViewById(R.id.tv_status);
        tv_sum = findViewById(R.id.tv_sum);

        iv_dice1 = findViewById(R.id.iv_dice_1);
        iv_dice2 = findViewById(R.id.iv_dice_2);
        //Roll
        b_roll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random random = new Random();
                int firstDice = random.nextInt(6) + 1;
                int secondDice = random.nextInt(6) + 1;
                //Calculate even or odd
                if (rb_def.isChecked()) {
                    setImages(firstDice, secondDice);
                } else {
                    setImages2(firstDice, secondDice);
                }
                int sum = firstDice + secondDice;
                int left = sum % 2;
                //Calculate the winner
                if (currentPick.equals("even")) {
                    if (left == 0) {
                        youPoint++;
                    }
                    if (left != 0) {
                        cpuPoint++;
                    }
                }
                if (currentPick.equals("odd")) {
                    if (left == 0) {
                        cpuPoint++;
                    }
                    if (left != 0) {
                        youPoint++;
                    }
                }
                //Calculate point
                tv_cpu.setText("CPU: " + cpuPoint);
                tv_you.setText("YOU: " + youPoint);
                tv_sum.setText(sum + "");

                b_roll.setVisibility(View.INVISIBLE);
                b_back.setVisibility(View.VISIBLE);
                b_even.setVisibility(View.VISIBLE);
                b_odd.setVisibility(View.VISIBLE);

                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate);
                iv_dice1.startAnimation(animation);
                iv_dice2.startAnimation(animation);
            }
        });
        //Even
        b_even.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentPick = "even";
                tv_status.setText(currentPick);

                b_roll.setVisibility(View.VISIBLE);
                b_back.setVisibility(View.INVISIBLE);
                b_even.setVisibility(View.INVISIBLE);
                b_odd.setVisibility(View.INVISIBLE);
            }
        });
        //Odd
        b_odd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentPick = "odd";
                tv_status.setText(currentPick);

                b_roll.setVisibility(View.VISIBLE);
                b_back.setVisibility(View.INVISIBLE);
                b_even.setVisibility(View.INVISIBLE);
                b_odd.setVisibility(View.INVISIBLE);
            }
        });
        //Back
        b_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void setImages(int dice1Num, int dice2Num) {
        //setPicture1
        switch (dice1Num) {
            case 1:
                iv_dice1.setImageResource(R.drawable.dice1);
                break;
            case 2:
                iv_dice1.setImageResource(R.drawable.dice2);
                break;
            case 3:
                iv_dice1.setImageResource(R.drawable.dice3);
                break;
            case 4:
                iv_dice1.setImageResource(R.drawable.dice4);
                break;
            case 5:
                iv_dice1.setImageResource(R.drawable.dice5);
                break;
            case 6:
                iv_dice1.setImageResource(R.drawable.dice6);
                break;
        }
        //setPicture2
        switch (dice2Num) {
            case 1:
                iv_dice2.setImageResource(R.drawable.dice1);
                break;
            case 2:
                iv_dice2.setImageResource(R.drawable.dice2);
                break;
            case 3:
                iv_dice2.setImageResource(R.drawable.dice3);
                break;
            case 4:
                iv_dice2.setImageResource(R.drawable.dice4);
                break;
            case 5:
                iv_dice2.setImageResource(R.drawable.dice5);
                break;
            case 6:
                iv_dice2.setImageResource(R.drawable.dice6);
                break;
        }
    }

    public void setImages2(int dice1Num, int dice2Num) {
        //setPicture1
        switch (dice1Num) {
            case 1:
                iv_dice1.setImageResource(R.drawable.dice1bw);
                break;
            case 2:
                iv_dice1.setImageResource(R.drawable.dice2bw);
                break;
            case 3:
                iv_dice1.setImageResource(R.drawable.dice3bw);
                break;
            case 4:
                iv_dice1.setImageResource(R.drawable.dice4bw);
                break;
            case 5:
                iv_dice1.setImageResource(R.drawable.dice5bw);
                break;
            case 6:
                iv_dice1.setImageResource(R.drawable.dice6bw);
                break;
        }
        //setPicture2
        switch (dice2Num) {
            case 1:
                iv_dice2.setImageResource(R.drawable.dice1bw);
                break;
            case 2:
                iv_dice2.setImageResource(R.drawable.dice2bw);
                break;
            case 3:
                iv_dice2.setImageResource(R.drawable.dice3bw);
                break;
            case 4:
                iv_dice2.setImageResource(R.drawable.dice4bw);
                break;
            case 5:
                iv_dice2.setImageResource(R.drawable.dice5bw);
                break;
            case 6:
                iv_dice2.setImageResource(R.drawable.dice6bw);
                break;
        }
    }
}
