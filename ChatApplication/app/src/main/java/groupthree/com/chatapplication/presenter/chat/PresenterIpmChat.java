package groupthree.com.chatapplication.presenter.chat;

import android.net.Uri;
import java.util.ArrayList;

public interface PresenterIpmChat {
    void getAccount(String uid);
    void getZoomChat(String idTalk);
    void eventLoadPicture();
    void sendChat(String idTalk, ArrayList<Uri> arrAnh, String mess);

}
