package groupthree.com.chatapplication.model;

import java.util.ArrayList;

public class StatusModel {
    private String id;
    public String post;
    public long time;
    private ArrayList<ImageModel> arrImageModel;
    private Account account;
    private ArrayList<Comment> arrComment;
    private ArrayList<Like> arrLike;

    public StatusModel() {
    }

    public StatusModel(String id, String post, long time, ArrayList<ImageModel> arrImageModel, Account account, ArrayList<Comment> arrComment, ArrayList<Like> arrLike) {
        this.id = id;
        this.post = post;
        this.time = time;
        this.arrImageModel = arrImageModel;
        this.account = account;
        this.arrComment = arrComment;
        this.arrLike = arrLike;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public ArrayList<ImageModel> getArrImageModel() {
        return arrImageModel;
    }

    public void setArrImageModel(ArrayList<ImageModel> arrImageModel) {
        this.arrImageModel = arrImageModel;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public ArrayList<Comment> getArrComment() {
        return arrComment;
    }

    public void setArrComment(ArrayList<Comment> arrComment) {
        this.arrComment = arrComment;
    }

    public ArrayList<Like> getArrLike() {
        return arrLike;
    }

    public void setArrLike(ArrayList<Like> arrLike) {
        this.arrLike = arrLike;
    }

    @Override
    public String toString() {
        return "StatusModel{" +
                "id='" + id + '\'' +
                ", post='" + post + '\'' +
                ", time=" + time +
                ", arrImageModel=" + arrImageModel +
                ", account=" + account +
                ", arrComment=" + arrComment +
                ", arrLike=" + arrLike +
                '}';
    }
}
