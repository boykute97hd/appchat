package groupthree.com.chatapplication.presenter.main.fragment.otherfragment;

import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import groupthree.com.chatapplication.model.Account;
import groupthree.com.chatapplication.view.main.fragement.other.ViewOther;

public class PresenterLogicOther implements PresenterIpmOther {
    ViewOther viewOther;
    private DatabaseReference mDatabase;
    private FirebaseUser mUser;


    public PresenterLogicOther(ViewOther viewOther) {
        this.viewOther = viewOther;
        mUser = FirebaseAuth.getInstance().getCurrentUser();
    }

    public ViewOther getViewOther() {
        return viewOther;
    }


    @Override
    public void getInfomationUser() {
        String uid = mUser.getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Accounts").child(uid);
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String dNameTemp = dataSnapshot.child("displayName").getValue().toString();
                String avatarTemp = dataSnapshot.child("avatar").getValue().toString();
                getViewOther().getDisplayName(dNameTemp);
                getViewOther().getAvatar(avatarTemp);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
