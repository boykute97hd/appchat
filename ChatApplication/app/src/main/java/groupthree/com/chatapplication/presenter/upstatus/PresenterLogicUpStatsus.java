package groupthree.com.chatapplication.presenter.upstatus;

import android.app.Dialog;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;

import groupthree.com.chatapplication.model.Account;
import groupthree.com.chatapplication.view.upstatus.ViewUpStatus;

public class PresenterLogicUpStatsus implements PresenterIpmUpStatus{
    private ViewUpStatus viewUpStatus;

    public PresenterLogicUpStatsus(ViewUpStatus viewUpStatus) {
        this.viewUpStatus = viewUpStatus;
    }

    public ViewUpStatus getViewUpStatus() {
        return viewUpStatus;
    }

    @Override
    public void loadAccount() {
        final String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        final DatabaseReference dataAccount = FirebaseDatabase.getInstance().getReference().child("Accounts").child(uid);
        dataAccount.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Account a = dataSnapshot.getValue(Account.class);
                a.setId(uid);
                getViewUpStatus().loadView(a);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void setEnableButtonPost(boolean enable) {
        getViewUpStatus().setEnableButtonPost(enable);
    }

    @Override
    public void upStatus(String text, ArrayList<Uri> arrUri) {
        getViewUpStatus().showDialogProgress();
        final String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        final DatabaseReference mData = FirebaseDatabase.getInstance().getReference();
        DatabaseReference databaseNews = mData.child("News").child(uid);
        final String idTemp = databaseNews.push().getKey();
        final DatabaseReference databaseNewTemp = databaseNews.child(idTemp);
        databaseNewTemp.child("post").setValue(text);
        databaseNewTemp.child("time").setValue(System.currentTimeMillis());
        if(arrUri.size()>0){
            for (Uri uri : arrUri) {
                final String idAnh = databaseNewTemp.child("image").push().getKey();
                final StorageReference storageUser = FirebaseStorage.getInstance().getReference().child("news").child(idAnh);
                storageUser.putFile(uri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                        if(task.isSuccessful()){
                            storageUser.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    final String downloadUrl = uri.toString();
                                    databaseNewTemp.child("image").child(idAnh).setValue(downloadUrl);
                                    DatabaseReference databaseReference = mData.child("Albums").child(uid);
                                    databaseReference.child("time").child(idAnh).setValue(downloadUrl);
                                    getViewUpStatus().hideDialogProgress();
                                    getViewUpStatus().finishActivity();

                                }
                            });
                        }
                    }
                });
            }
        }else {
            getViewUpStatus().hideDialogProgress();
            getViewUpStatus().finishActivity();
        }

    }
}
