package groupthree.com.chatapplication.view.setting;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import groupthree.com.chatapplication.R;

public class InfomationActivity extends AppCompatActivity {
    ImageButton imgbBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_infomation);
        imgbBack = findViewById(R.id.imgbBack);
        imgbBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_exitintent,R.anim.anim_exit_exit);
    }
}
