package groupthree.com.chatapplication.model;

import java.util.ArrayList;

public class MesseageModel {
    private String id;
    public long time;
    public String mess;
    public String from;
    private ArrayList<ImageModel> arrImage;

    public MesseageModel() {
    }


    public MesseageModel(String id, long time, String mess, String from, ArrayList<ImageModel> arrImage) {
        this.id = id;
        this.time = time;
        this.mess = mess;
        this.from = from;
        this.arrImage = arrImage;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getMess() {
        return mess;
    }

    public void setMess(String mess) {
        this.mess = mess;
    }

    public ArrayList<ImageModel> getArrImage() {
        return arrImage;
    }

    public void setArrImage(ArrayList<ImageModel> arrImage) {
        this.arrImage = arrImage;
    }

    @Override
    public String toString() {
        return "MesseageModel{" +
                "id='" + id + '\'' +
                ", time=" + time +
                ", mess='" + mess + '\'' +
                ", from='" + from + '\'' +
                ", arrImage=" + arrImage +
                '}';
    }
}
