package groupthree.com.chatapplication.view.listrequest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import groupthree.com.chatapplication.R;
import groupthree.com.chatapplication.model.FriendRequest;
import groupthree.com.chatapplication.presenter.listrequest.PresenterIpmRequest;
import groupthree.com.chatapplication.presenter.listrequest.PresenterLogicRequest;

public class RequestActivity extends AppCompatActivity implements ViewRequest{
    private ImageButton imgBack;
    private RecyclerView rvRequest;
    private TextView tvAlert;
    private ArrayList<FriendRequest> arrRequest;
    private PresenterLogicRequest presenterLogicRequest;
    private RecyclerView.LayoutManager layoutManager;
    private RequestAdapter requestAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request);
        init();
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        presenterLogicRequest.loadListRequest();


    }

    private void loadUi() {
        if(arrRequest.size() == 0){
            tvAlert.setVisibility(View.VISIBLE);
        }else {
            tvAlert.setVisibility(View.GONE);
        }
    }

    private void init() {
        imgBack = findViewById(R.id.imgbBack);
        rvRequest = findViewById(R.id.rvRequest);
        tvAlert = findViewById(R.id.tvAlert);
        arrRequest =new ArrayList<>();
        presenterLogicRequest = new PresenterLogicRequest(this);
        layoutManager = new LinearLayoutManager(RequestActivity.this);
        rvRequest.setLayoutManager(layoutManager);
        requestAdapter = new RequestAdapter(arrRequest,R.layout.layout_item_request,presenterLogicRequest,RequestActivity.this);
        rvRequest.setAdapter(requestAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_exitintent,R.anim.anim_exit_exit);
    }

    @Override
    public void loadData(ArrayList<FriendRequest> temp) {
        arrRequest.clear();
        if(temp!=null) {
            arrRequest.addAll(temp);
        }
        requestAdapter.notifyDataSetChanged();
        loadUi();
    }
}
