package groupthree.com.chatapplication.view.upstatus;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import groupthree.com.chatapplication.R;
import groupthree.com.chatapplication.model.Account;
import groupthree.com.chatapplication.presenter.upstatus.PresenterLogicUpStatsus;
import groupthree.com.chatapplication.view.signup.SignUpActivity;

public class UpStatusActivity extends AppCompatActivity implements ViewUpStatus {
    //Control
    private CircleImageView imgAvt;
    private TextView tvDisplayName;
    private Button btnPost;
    private EditText edtContenPost;
    private ImageButton imgbBack, imgbSelectPicture;
    private RecyclerView rvPickSelect;
    private RecyclerView.LayoutManager layoutPicSelected;
    private ArrayList<Uri> arrPickSelected;
    private Dialog dialogProgress;
    // Presenter
    private PresenterLogicUpStatsus presenterLogicUpStatsus;
    //Intent
    private static final int GALLERY_PICK = 1;
    //Adapter
    private PictureSelectedAdapter pictureSelectedAdapter;
    //Bien
    private boolean isHavePost = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_up_status);
        init();
        presenterLogicUpStatsus.loadAccount();
        setEnableButtonPost(false);
        //intent
        int picture = getIntent().getIntExtra("picture",0);
        if(picture == 1){
            Intent iGallery = new Intent();
            iGallery.setType("image/*");
            iGallery.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            iGallery.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(iGallery,"Chọn ảnh"),GALLERY_PICK);
        }
        //Su kien post ko co chư ko cho dang

        edtContenPost.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()==0){
                    setEnableButtonPost(false);
                }else {
                    setEnableButtonPost(true);
                }
                isHavePost = true;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        //Back
        imgbBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        //Chon anh
        imgbSelectPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iGallery = new Intent();
                iGallery.setType("image/*");
                iGallery.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                iGallery.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(iGallery,"Chọn ảnh"),GALLERY_PICK);

            }
        });
        //Post
        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = edtContenPost.getText().toString();
                presenterLogicUpStatsus.upStatus(text,arrPickSelected);
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == GALLERY_PICK && resultCode == RESULT_OK){
            if (data.getClipData() != null) {
                int count = data.getClipData().getItemCount();
                for (int i = 0; i < count; i++) {
                    Uri imageUri = data.getClipData().getItemAt(i).getUri();
                    arrPickSelected.add(imageUri);
                }
                setEnableButtonPost(true);
                pictureSelectedAdapter.notifyDataSetChanged();
            }else if(data.getData() != null) {
                Uri imagePath = data.getData();
                arrPickSelected.add(imagePath);
                pictureSelectedAdapter.notifyDataSetChanged();
                setEnableButtonPost(true);
            }
            isHavePost = true;
        }
    }

    private void init() {
        imgAvt = findViewById(R.id.imgAvatar);
        tvDisplayName = findViewById(R.id.tvDisplayName);
        btnPost = findViewById(R.id.btnPost);
        edtContenPost = findViewById(R.id.edtContentPost);
        imgbBack = findViewById(R.id.imgbBack);
        imgbSelectPicture = findViewById(R.id.btnSelectPicture);
        presenterLogicUpStatsus = new PresenterLogicUpStatsus(this);
        rvPickSelect = findViewById(R.id.rvPickSelect);
        layoutPicSelected = new LinearLayoutManager(UpStatusActivity.this);
        rvPickSelect.setLayoutManager(layoutPicSelected);
        arrPickSelected = new ArrayList<>();
        pictureSelectedAdapter = new PictureSelectedAdapter(arrPickSelected,R.layout.layout_item_pick,presenterLogicUpStatsus);
        rvPickSelect.setAdapter(pictureSelectedAdapter);
        dialogProgress= new Dialog(UpStatusActivity.this);
    }

    @Override
    public void loadView(final Account account) {
        tvDisplayName.setText(account.displayName);
        if(account.avatar.equals("default")){
            imgAvt.setBackgroundResource(R.drawable.avt);
        }else {
            Picasso.get().load(account.avatar).networkPolicy(NetworkPolicy.OFFLINE).placeholder(R.drawable.anhbia).into(imgAvt, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {
                    Picasso.get().load(account.avatar).placeholder(R.drawable.anhbia).into(imgAvt);
                }
            });
        }
    }

    @Override
    public void setEnableButtonPost(boolean enable) {
        if(enable == false){
            btnPost.setEnabled(false);
            btnPost.setTextColor(getResources().getColor(R.color.color_disable));
        }else {
            btnPost.setEnabled(true);
            btnPost.setTextColor(getResources().getColor(R.color.white));
        }
    }

    @Override
    public void finishActivity() {
        finish();
        overridePendingTransition(R.anim.anim_exitintent,R.anim.anim_exit_exit);
    }

    @Override
    public void showDialogProgress() {
        dialogProgress.setContentView(R.layout.dialog_progress);
        dialogProgress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogProgress.show();
    }

    @Override
    public void hideDialogProgress() {
        dialogProgress.dismiss();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if(isHavePost == false) {
            finishActivity();
        }else {
            AlertDialog.Builder builder = new AlertDialog.Builder(UpStatusActivity.this,R.style.AlertDialogCustom);
            builder.setMessage("Bạn có muốn hủy bài biêt này ?");
            builder.setPositiveButton("Có", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finishActivity();
                }
            });
            builder.setNegativeButton("Không", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.create().show();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();


    }
}
