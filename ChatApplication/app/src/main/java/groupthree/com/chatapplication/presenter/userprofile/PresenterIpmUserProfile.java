package groupthree.com.chatapplication.presenter.userprofile;

public interface PresenterIpmUserProfile {
    void loadUserProfile();
    void loadUserTimeline();
}
