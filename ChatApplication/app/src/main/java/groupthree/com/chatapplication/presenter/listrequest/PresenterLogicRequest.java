package groupthree.com.chatapplication.presenter.listrequest;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import groupthree.com.chatapplication.model.Account;
import groupthree.com.chatapplication.model.FriendRequest;
import groupthree.com.chatapplication.view.listrequest.ViewRequest;

public class PresenterLogicRequest implements PresenterIpmRequest {
    ViewRequest viewRequest;

    public PresenterLogicRequest(ViewRequest viewRequest) {
        this.viewRequest = viewRequest;
    }

    public ViewRequest getViewRequest() {
        return viewRequest;
    }

    @Override
    public void loadListRequest() {
        final String currentid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.child("FriendsRequests").hasChild(currentid)){
                    DataSnapshot dataSnapshot1 = dataSnapshot.child("FriendsRequests").child(currentid);
                    ArrayList<FriendRequest> arrRequest = new ArrayList<>();
                    for(DataSnapshot snapshot: dataSnapshot1.getChildren()){
                        if(snapshot.child("request_type").getValue().equals("received")){
                            FriendRequest friendRequest = snapshot.getValue(FriendRequest.class);
                            friendRequest.setId(snapshot.getKey());
                            DataSnapshot mAccount = dataSnapshot.child("Accounts").child(friendRequest.getId());
                            Account a = mAccount.getValue(Account.class);
                            friendRequest.setAccount(a);
                            arrRequest.add(friendRequest);
                        }
                    }
                    getViewRequest().loadData(arrRequest);
                }else {
                    getViewRequest().loadData(null);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

        DatabaseReference mDataRequest  = FirebaseDatabase.getInstance().getReference();
        mDataRequest.addValueEventListener(valueEventListener);
    }

    @Override
    public void allowRequest(final String uid) {
        final String currentId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference mData = FirebaseDatabase.getInstance().getReference().child("FriendsRequests").child(currentId);
        mData.child(uid).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                final DatabaseReference mDataFriend = FirebaseDatabase.getInstance().getReference().child("FriendsRequests").child(uid);
                mDataFriend.child(currentId).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        DatabaseReference mDataUserFriends = FirebaseDatabase.getInstance().getReference().child("Friends").child(currentId);
                        mDataUserFriends.child(uid).setValue(System.currentTimeMillis());
                        DatabaseReference mDataFriendsUser = FirebaseDatabase.getInstance().getReference().child("Friends").child(uid);
                        mDataFriendsUser.child(currentId).setValue(System.currentTimeMillis());
                        loadListRequest();
                    }
                });
            }
        });
    }

    @Override
    public void delicneRequest(final String uid) {
        final String currentId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference mData = FirebaseDatabase.getInstance().getReference().child("FriendsRequests").child(currentId);
        mData.child(uid).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                DatabaseReference mDataFriend = FirebaseDatabase.getInstance().getReference().child("FriendsRequests").child(uid);
                mDataFriend.child(currentId).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        loadListRequest();
                    }
                });
            }
        });
    }
}
