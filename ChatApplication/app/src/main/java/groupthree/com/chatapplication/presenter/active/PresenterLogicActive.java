package groupthree.com.chatapplication.presenter.active;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.TwitterAuthCredential;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import groupthree.com.chatapplication.view.active.ViewActive;

public class PresenterLogicActive implements PresenterIpmActive {

    private DatabaseReference mDatabase;
    private FirebaseUser mUser;
    private ViewActive viewActive;

    public PresenterLogicActive(ViewActive viewActive) {
        this.viewActive = viewActive;
        mUser = FirebaseAuth.getInstance().getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Accounts").child(mUser.getUid());
    }

    public ViewActive getViewActive() {
        return viewActive;
    }

    public void setViewActive(ViewActive viewActive) {
        this.viewActive = viewActive;
    }

    @Override
    public String checkActiveAccount() {
        mUser.reload();
        return String.valueOf(mUser.isEmailVerified());
    }

    @Override
    public void resendCodeVerify() {
        mUser.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    getViewActive().resendSucccess();
                }else {
                    getViewActive().resendFail();
                }
            }
        });
    }

    @Override
    public void enabaleButtonContinue() {
        mDatabase.child("active").setValue("true").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    getViewActive().actived();
                }else {
                    getViewActive().notActived();
                }
            }
        });
    }
}
