package groupthree.com.chatapplication.model;

public class Like {
    private Account account;
    private long time;

    public Like() {
    }

    public Like(Account account, long time) {
        this.account = account;
        this.time = time;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
