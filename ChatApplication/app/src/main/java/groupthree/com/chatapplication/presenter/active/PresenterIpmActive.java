package groupthree.com.chatapplication.presenter.active;

public interface PresenterIpmActive
{
    String checkActiveAccount();
    void resendCodeVerify();
    void enabaleButtonContinue();
}
