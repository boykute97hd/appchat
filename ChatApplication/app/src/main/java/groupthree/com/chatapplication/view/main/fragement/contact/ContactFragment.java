package groupthree.com.chatapplication.view.main.fragement.contact;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import groupthree.com.chatapplication.R;
import groupthree.com.chatapplication.model.ContactModel;
import groupthree.com.chatapplication.presenter.main.fragment.contactfragment.PresenterLogicContact;
import groupthree.com.chatapplication.view.listrequest.RequestActivity;
import groupthree.com.chatapplication.view.main.adapter.ContactAdapter;
import groupthree.com.chatapplication.view.searchfriend.SearchFriendActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactFragment extends Fragment implements ViewContact{
    private EditText edtSearch;
    private RecyclerView recyclerView;
    private List<ContactModel> arrContactModel;
    private ContactAdapter contactAdapter;
    private ImageButton imgRequest;
    private SwipeRefreshLayout srlContact;
    PresenterLogicContact presenterLogicContact;
    public ContactFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_contact, container, false);
        init(view);
        edtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iSearch = new Intent(view.getContext(), SearchFriendActivity.class);
                startActivity(iSearch);
                getActivity().overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
            }
        });
        imgRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(view.getContext(),RequestActivity.class);
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
            }
        });
        srlContact.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenterLogicContact.reload();
            }
        });
        return view;
    }

    private void init(View view) {
        edtSearch = view.findViewById(R.id.edtSearch);
        recyclerView = view.findViewById(R.id.rvListFriend);
        imgRequest = view.findViewById(R.id.btnRequest);
        arrContactModel = new ArrayList<>();
        presenterLogicContact = new PresenterLogicContact(this);
        srlContact = view.findViewById(R.id.srlContact);
    }

    @Override
    public void onStart() {
        super.onStart();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        contactAdapter = new ContactAdapter(arrContactModel,R.layout.item_layout_contact,getContext());
        recyclerView.setAdapter(contactAdapter);
        presenterLogicContact.getList();

    }

    @Override
    public void listFriend(List<ContactModel> arrFriend) {
        arrContactModel.clear();
        for(ContactModel temp:arrFriend){
            arrContactModel.add(temp);
            contactAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void setRefresh(boolean enable) {
        Handler handler =new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                srlContact.setRefreshing(false);
            }
        },2000);
    }
}
