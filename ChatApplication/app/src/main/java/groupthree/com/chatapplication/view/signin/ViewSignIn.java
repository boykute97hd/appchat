package groupthree.com.chatapplication.view.signin;

public interface ViewSignIn {
    void showDialog();
    void hideDialog();
    void loginSucccess();
    void loginFail(int error);
}
