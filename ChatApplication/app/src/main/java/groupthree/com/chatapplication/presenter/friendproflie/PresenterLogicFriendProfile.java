package groupthree.com.chatapplication.presenter.friendproflie;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import groupthree.com.chatapplication.model.Account;
import groupthree.com.chatapplication.view.friendprofile.ViewFriendProfile;

public class PresenterLogicFriendProfile implements PresenterIpmFriendProfile {
    private ViewFriendProfile viewFriendProfile;
    private DatabaseReference mData;
    public PresenterLogicFriendProfile(ViewFriendProfile viewFriendProfile) {
        this.viewFriendProfile = viewFriendProfile;
    }

    public ViewFriendProfile getViewFriendProfile() {
        return viewFriendProfile;
    }

    @Override
    public void loadFriendProfile(final String uid) {
        mData = FirebaseDatabase.getInstance().getReference().child("Accounts").child(uid);
        mData.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Account a = dataSnapshot.getValue(Account.class);
                a.setId(uid);
                getViewFriendProfile().getAnhBia(a.wallpaper);
                getViewFriendProfile().getBirthday(a.birthday);
                getViewFriendProfile().getDisplayName(a.displayName);
                getViewFriendProfile().getGender(a.gender);
                getViewFriendProfile().loadAvatar(a.avatar);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void loadUI(final String uid) {
        mData = FirebaseDatabase.getInstance().getReference();
        final String currentId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference mDataFriendRequest = mData.child("FriendsRequests");
        //
        //Kiểm tra danh sách lời mời
        //request_type = sent thì là đã gửi
        //request_type = received thì là nhận
        //Sau đó LoadUI giao diện FriendProfileActivity
        //
        mDataFriendRequest.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild(currentId)) {
                    DataSnapshot dataSnapshot1 = dataSnapshot.child(currentId);
                    for (DataSnapshot snapshot : dataSnapshot1.getChildren()) {
                        Log.d("snapshotkey", snapshot.getKey());
                        if (snapshot.getKey().equals(uid)) {
                            String type = snapshot.child("request_type").getValue().toString();
                            if(type.equals("received")){
                                getViewFriendProfile().loadUIFriend(1);
                            }else if(type.equals("sent")){
                                getViewFriendProfile().loadUIFriend(2);
                            }else {
                                getViewFriendProfile().loadUIFriend(0);
                            }
                            break;
                        }
                    }
                }else {
                    //
                    //Load trong danh sách Friends nếu Ko có FriendsRequest
                    //
                    DatabaseReference mDataFriends = mData.child("Friends");
                    mDataFriends.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if(dataSnapshot.hasChild(currentId)){
                                DataSnapshot dataSnapshot1 = dataSnapshot.child(currentId);
                                for(DataSnapshot snapshot : dataSnapshot1.getChildren()){
                                    if(snapshot.getKey().equals(uid)){
                                        getViewFriendProfile().loadUIFriend(3);
                                        break;
                                    }
                                }
                            }
                            else {
                                getViewFriendProfile().loadUIFriend(0);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }

    @Override
    public void loadFriendTimeline(final String uid) {

    }

    @Override
    public void sendRequestFriend(String uid) {
        String currentId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        mData = FirebaseDatabase.getInstance().getReference().child("FriendsRequests").child(currentId);
        mData.child(uid).child("request_type").setValue("sent");
        mData.child(uid).child("time").setValue(System.currentTimeMillis());
        DatabaseReference mDataFriend = FirebaseDatabase.getInstance().getReference().child("FriendsRequests").child(uid);
        mDataFriend.child(currentId).child("request_type").setValue("received");
        mDataFriend.child(currentId).child("time").setValue(System.currentTimeMillis());
        getViewFriendProfile().loadUIFriend(2);
    }

    @Override
    public void removeRequestFriend(final String uid) {
        final String currentId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        mData = FirebaseDatabase.getInstance().getReference().child("FriendsRequests").child(currentId);
        mData.child(uid).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                DatabaseReference mDataFriend = FirebaseDatabase.getInstance().getReference().child("FriendsRequests").child(uid);
                mDataFriend.child(currentId).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        getViewFriendProfile().loadUIFriend(0);
                    }
                });
            }
        });
    }

    @Override
    public void allowRequest(final String uid) {
        final String currentId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        mData = FirebaseDatabase.getInstance().getReference().child("FriendsRequests").child(currentId);
        mData.child(uid).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                final DatabaseReference mDataFriend = FirebaseDatabase.getInstance().getReference().child("FriendsRequests").child(uid);
                mDataFriend.child(currentId).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        DatabaseReference mDataUserFriends = FirebaseDatabase.getInstance().getReference().child("Friends").child(currentId);
                        mDataUserFriends.child(uid).setValue(System.currentTimeMillis());
                        DatabaseReference mDataFriendsUser = FirebaseDatabase.getInstance().getReference().child("Friends").child(uid);
                        mDataFriendsUser.child(currentId).setValue(System.currentTimeMillis());
                        getViewFriendProfile().loadUIFriend(3);
                    }
                });
            }
        });
    }

    @Override
    public void declineRequest(final String uid) {
        final String currentId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        mData = FirebaseDatabase.getInstance().getReference().child("FriendsRequests").child(currentId);
        mData.child(uid).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                DatabaseReference mDataFriend = FirebaseDatabase.getInstance().getReference().child("FriendsRequests").child(uid);
                mDataFriend.child(currentId).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        getViewFriendProfile().loadUIFriend(0);
                    }
                });
            }
        });
    }

    @Override
    public void removeFriends(final String uid) {
        final String currentId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        mData = FirebaseDatabase.getInstance().getReference().child("Friends");
        final DatabaseReference mDataUserFriend = mData.child(currentId);
        mDataUserFriend.child(uid).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                DatabaseReference mDataFriendUser = mData.child(uid);
                mDataFriendUser.child(currentId).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        getViewFriendProfile().loadUIFriend(0);
                    }
                });
            }
        });

    }
}
