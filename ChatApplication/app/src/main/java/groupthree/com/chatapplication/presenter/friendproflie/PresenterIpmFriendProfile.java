package groupthree.com.chatapplication.presenter.friendproflie;

public interface PresenterIpmFriendProfile {
    void loadFriendProfile(String uid);
    void loadUI(String uid);
    void loadFriendTimeline(String uid);
    void sendRequestFriend(String uid);
    void removeRequestFriend(String uid);
    void allowRequest(String uid);
    void declineRequest(String uid);
    void removeFriends(String uid);
}
