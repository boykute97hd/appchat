package groupthree.com.chatapplication.presenter.splashscreen;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import groupthree.com.chatapplication.view.splashscreen.ViewSplashScreen;

public class PresenterLogicSplashScreen implements PresenterSplashScreen {
    private ViewSplashScreen viewSplashScreen;
    private FirebaseUser currentUser;
    private Handler handler;
    private DatabaseReference mDatabase;
    public PresenterLogicSplashScreen(ViewSplashScreen viewSplashScreen) {
        this.viewSplashScreen = viewSplashScreen;
        handler = new Handler();
    }

    @Override
    public void checkCurrenUser() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                currentUser = FirebaseAuth.getInstance().getCurrentUser();
                if(currentUser != null){
                    viewSplashScreen.haveCurrenUser();
                }else {
                    viewSplashScreen.noCurrentUser();
                }
            }
        },2000);

    }
}
