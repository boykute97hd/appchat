package groupthree.com.chatapplication.view.changeinfomation;

import groupthree.com.chatapplication.model.Account;

public interface ViewChangeInfomation {
    void updateAvatarSucccess();
    void updateAvatarFail(String messageFail);
    void updateWallpaperSuccsess();
    void updateWallpaperFail(String messageFail);
    void updateProfileSucccess();
    void updateProfileFai(String messageFail);
    void showInforOld(Account account);
    void showDialog();
    void hideDialog();
}
