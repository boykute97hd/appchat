package groupthree.com.chatapplication.view.main.fragement.other;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import groupthree.com.chatapplication.R;
import groupthree.com.chatapplication.presenter.main.fragment.otherfragment.PresenterLogicOther;
import groupthree.com.chatapplication.view.game.MainGame;
import groupthree.com.chatapplication.view.searchfriend.SearchFriendActivity;
import groupthree.com.chatapplication.view.setting.SettingActivity;
import groupthree.com.chatapplication.view.userprofile.UserProfile;

/**
 * A simple {@link Fragment} subclass.
 */
public class OtherFragment extends Fragment implements ViewOther {
    private LinearLayout llTrangCaNhan;
    private ImageButton imgSetting;
    private TextView tvDisplayName;
    private ImageView imgAvatar;
    private PresenterLogicOther presenterLogicOther;
    private EditText edtSearch;
    private Button btnGame;
    public OtherFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_other, container, false);
        init(view);
        imgSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iSetting = new Intent(view.getContext(), SettingActivity.class);
                startActivity(iSetting);
                getActivity().overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
            }
        });
        presenterLogicOther.getInfomationUser();
        llTrangCaNhan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(view.getContext(), UserProfile.class);
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
            }
        });
        edtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iSeacrch = new Intent(view.getContext(), SearchFriendActivity.class);
                startActivity(iSeacrch);
                getActivity().overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
            }
        });
        btnGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iGame = new Intent(view.getContext(), MainGame.class);
                startActivity(iGame);
                getActivity().overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
            }
        });
        return view;
    }

    private void init(View view) {
        imgSetting = view.findViewById(R.id.imgSetting);
        tvDisplayName = view.findViewById(R.id.tvDisplayname);
        imgAvatar = view.findViewById(R.id.imgAvatar);
        presenterLogicOther = new PresenterLogicOther(this);
        llTrangCaNhan = view.findViewById(R.id.llTrangCaNhan);
        edtSearch = view.findViewById(R.id.edtSearch);
        btnGame = view.findViewById(R.id.btnGame);
    }


    @Override
    public void getDisplayName(String displayName) {
        tvDisplayName.setText(displayName);
    }

    @Override
    public void getAvatar(String avatar) {
        if(avatar.equals("default")){
            imgAvatar.setImageResource(R.drawable.avt);
        }else {
            Picasso.get().load(avatar).into(imgAvatar);
        }
    }
}
