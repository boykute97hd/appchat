package groupthree.com.chatapplication.view.main.fragement.group;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import groupthree.com.chatapplication.R;
import groupthree.com.chatapplication.view.searchfriend.SearchFriendActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class GroupFragment extends Fragment {

    private EditText edtSearch;
    public GroupFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_group, container, false);
        init(view);
        edtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iSearch = new Intent(view.getContext(), SearchFriendActivity.class);
                startActivity(iSearch);
                getActivity().overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
            }
        });
        return view;
    }

    private void init(View view) {
        edtSearch = view.findViewById(R.id.edtSearch);
    }

}
