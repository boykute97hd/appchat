package groupthree.com.chatapplication.view.game;

import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import groupthree.com.chatapplication.R;

public class reflex extends AppCompatActivity {
    Button b_start, b_main, b_back;
    long startTime, endtime, currenttime, besttime = 10000;

    TextView tv_infor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reflex);
        mainApp();
    }

    public void mainApp() {
        b_start = findViewById(R.id.b_start);
        b_main = findViewById(R.id.b_main);
        b_back = findViewById(R.id.b_back);
        tv_infor = findViewById(R.id.tv_infor);

        b_start.setEnabled(true);
        b_main.setEnabled(false);

        tv_infor.setText("Best Time: " + besttime + " ms");

        b_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        b_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b_start.setEnabled(false);
                b_main.setText("");

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startTime = System.currentTimeMillis();
                        b_main.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.blue));
                        b_main.setText("Press");
                        b_main.setEnabled(true);
                    }
                }, 2000);
            }
        });

        b_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endtime = System.currentTimeMillis();
                currenttime = endtime - startTime;
                b_main.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.red));
                b_main.setText(currenttime + "ms");
                b_main.setEnabled(false);
                b_start.setEnabled(true);

                if (currenttime < besttime) {
                    besttime = currenttime;
                    tv_infor.setText("Best Time: " + besttime + " ms");

                }
            }
        });
    }
}
