package groupthree.com.chatapplication.view.chat;

import android.content.Context;
import android.content.Intent;
import android.inputmethodservice.Keyboard;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyboardShortcutGroup;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import groupthree.com.chatapplication.R;
import groupthree.com.chatapplication.model.Account;
import groupthree.com.chatapplication.model.MesseageModel;
import groupthree.com.chatapplication.presenter.chat.PresenterLogicChat;

public class ChatActivity extends AppCompatActivity implements ViewChat {
    private ImageView btnInsertEmotion;
    private boolean checkOpenEmotion;
    //View Controller
    private ImageButton imgbBack, imgbCall, imgbVideoCall, imgbOpenGallery, imgbSend;
    private TextView tvDisplayName, tvTrangThai;
    private EditText edtMesseage;
    private LinearLayout layoutGallery;
    private RecyclerView rvAnhSelected, rvMessage;
    private RecyclerView.LayoutManager layoutPicSelected;
    private RecyclerView.LayoutManager layoutChatManager;
    //Presenter
    PresenterLogicChat presenterLogicChat;
    private String uidTemp = null;
    //Array
    private ArrayList<Uri> arrAnh;
    private ArrayList<MesseageModel> arrMessage;
    //Adapter
    private PictureAdapter pictureAdapter;
    private ChatAdapter chatAdapter;
    //Intent
    private static final int GALLERY_PICK = 1;
    private GridView gvEmotion;
    private CardView cardEmotion;
    String[] arrEmotion = new String[]{"0x1F601","0x1F602","0x1F603","0x1F604","0x1F605"
            ,"0x1F606","0x1F609","0x1F60A","0x1F60B","0x1F60C","0x1F60D","0x1F60F","0x1F612"
            ,"0x1F613","0x1F614","0x1F616","0x1F618","0x1F61A","0x1F61C","0x1F61D","0x1F61E"
            ,"0x1F620","0x1F621","0x1F622","0x1F623","0x1F624","0x1F625","0x1F628","0x1F629"
            ,"0x1F62A","0x1F62B","0x1F62D","0x1F630","0x1F631","0x1F632","0x1F633","0x1F635","0x1F637"
            ,"0x1F385","0x1F471","0x1F473","0x1F575","0x1F46E","0x1F475","0x1F467","0x1F649"
            ,"0x1F472","0x1F930","0x1F64D","0x1F645","0x1F646","0x1F6B6","0x1F3C3","0x1F483"
            ,"0x1F57A","0x1F46F","0x1F46B","0x1F48F","0x1F4AA","0x1F448","0x1F449","0x1F446"
            ,"0x1F447","0x1F91E","0x1F596","0x1F590","0x1F44A","0x1F64F","0x1F496","0x1F4A3"};
    private ListEmotionAdapter emotionAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        init();
        imgbBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        //Get ID Account
        uidTemp =  getIntent().getStringExtra("acc_id");
        //load data toolbar
        presenterLogicChat.getAccount(uidTemp);
        //button open gallery picture
        imgbOpenGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iGallery = new Intent();
                iGallery.setType("image/*");
                iGallery.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                iGallery.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(iGallery,"Chọn ảnh"),GALLERY_PICK);

            }
        });
        emotionAdapter = new ListEmotionAdapter(this,R.layout.item_emotion,arrEmotion);
        gvEmotion.setAdapter(emotionAdapter);
        checkOpenEmotion = false;
edtMesseage.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        cardEmotion.setVisibility(View.GONE);
    }
});
        gvEmotion.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String txt="";

                String[] arr = arrEmotion[i].split(" ");
                for (int x = 0; x < arr.length; x++) {

                    int u = Integer.parseInt(arr[x].substring(2), 16);
                    txt = txt + " " + getEmojiByUnicode(u);

                }
                edtMesseage.setText(edtMesseage.getText().toString() + " " + txt);


            }
        });

        btnInsertEmotion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkOpenEmotion){
                    cardEmotion.setVisibility(View.GONE);
                    checkOpenEmotion = false;
                }else {
                    cardEmotion.setVisibility(View.VISIBLE);

                   hideKeyboard();
                    checkOpenEmotion = true;
                }
            }
        });
        imgbSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mess = edtMesseage.getText().toString().trim();
                presenterLogicChat.sendChat(uidTemp,arrAnh,mess);
                hideLayoutPicture();
                arrAnh.clear();
                edtMesseage.getText().clear();

            }
        });

        presenterLogicChat.getZoomChat(uidTemp);
    }
    private void hideKeyboard(){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

    }
    public String getEmojiByUnicode(int unicode) {
        return new String(Character.toChars(unicode));
    }
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_PICK && resultCode == RESULT_OK) {
            if (data.getClipData() != null) {
                int count = data.getClipData().getItemCount();
                for (int i = 0; i < count; i++) {
                    Uri imageUri = data.getClipData().getItemAt(i).getUri();
                    arrAnh.add(imageUri);
                }
                pictureAdapter.notifyDataSetChanged();
                layoutGallery.setVisibility(View.VISIBLE);
            }else if(data.getData() != null) {
                Uri imagePath = data.getData();
                arrAnh.add(imagePath);
                pictureAdapter.notifyDataSetChanged();
                layoutGallery.setVisibility(View.VISIBLE);
            }
        }
    }

    private void init() {
        gvEmotion = findViewById(R.id.gvEmotion);
        cardEmotion = findViewById(R.id.cardEmotion);
        imgbBack = findViewById(R.id.imgbBack);
        imgbCall = findViewById(R.id.imgbCall);
        imgbVideoCall = findViewById(R.id.imgbVideo);
        tvDisplayName = findViewById(R.id.tvDisplay);
        tvTrangThai = findViewById(R.id.tvTrangThai);
        presenterLogicChat = new PresenterLogicChat(this);
        edtMesseage = findViewById(R.id.edtMesseage);
        imgbOpenGallery = findViewById(R.id.btnSendImage);
        btnInsertEmotion = findViewById(R.id.btnInsertEmotion);
        layoutGallery = findViewById(R.id.layoutGallery);
        imgbSend = findViewById(R.id.btnSend);
        arrAnh = new ArrayList<>();
        rvAnhSelected = findViewById(R.id.rvAnh);
        layoutPicSelected = new LinearLayoutManager(ChatActivity.this,LinearLayoutManager.HORIZONTAL,false);
        rvAnhSelected.setLayoutManager(layoutPicSelected);
        pictureAdapter = new PictureAdapter(arrAnh,R.layout.layout_picture_pick,presenterLogicChat);
        rvAnhSelected.setAdapter(pictureAdapter);
        arrMessage = new ArrayList<>();
        layoutChatManager = new LinearLayoutManager(ChatActivity.this);
        rvMessage = findViewById(R.id.rvMesseage);
        rvMessage.setLayoutManager(layoutChatManager);
        chatAdapter = new ChatAdapter(arrMessage,R.layout.layout_itemchat,ChatActivity.this);

        rvMessage.setAdapter(chatAdapter);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_exitintent,R.anim.anim_exit_exit);
    }

    @Override
    public void loadInfo(Account a) {
        tvDisplayName.setText(a.displayName);
        if(a.online.equals("Online")){
            tvTrangThai.setText("Đang hoạt động");
        }else {
            long temp = Long.parseLong(a.online);
            Date date = new Date();
            Date time = new Date(temp);
            long tempTime = 0;
            String unit = "";
            if(TimeUnit.MILLISECONDS.toMinutes(date.getTime()-time.getTime())<60){
                tempTime = TimeUnit.MILLISECONDS.toMinutes(date.getTime()-time.getTime());
                unit = "phút";

            }else {
                if(TimeUnit.MICROSECONDS.toHours(date.getTime()-time.getTime())<24){
                    tempTime = TimeUnit.MICROSECONDS.toHours(date.getTime()-time.getTime());
                    unit = "giờ";
                }else {
                    tempTime = 0;
                }
            }
            if(tempTime == 0){
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy");
                tvTrangThai.setText("Hoạt động lúc "+simpleDateFormat.format(time));
            }else {
                tvTrangThai.setText(tempTime + " " + unit+ " trước");
            }
        }
    }

    @Override
    public void loadZoomChat(ArrayList<MesseageModel> arrMess) {
        this.arrMessage.clear();
        if(arrMess!=null && arrMess.size() !=0 ){
            this.arrMessage.addAll(arrMess);
        }
        chatAdapter.notifyDataSetChanged();
        rvMessage.scrollToPosition(arrMessage.size()-1);
    }


    @Override
    public void hideLayoutPicture() {
        layoutGallery.setVisibility(View.GONE);
    }
}
