package groupthree.com.chatapplication.view.game;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import groupthree.com.chatapplication.R;

public class MainGame extends AppCompatActivity {

    Button btg1, btg2, btg3, btg4, btg5, btg6;
    ImageButton imgbBack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_game);
        mainApp();
        imgbBack = findViewById(R.id.imgbBack);
        imgbBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void mainApp() {
        btg1 = findViewById(R.id.b_game1);
        btg2 = findViewById(R.id.b_game2);
        btg3 = findViewById(R.id.b_game3);
        btg4 = findViewById(R.id.b_game4);
        btg5 = findViewById(R.id.b_game5);
        btg6 = findViewById(R.id.b_game6);

        btg1.setBackgroundColor(getResources().getColor(R.color.white));
        btg2.setBackgroundColor(getResources().getColor(R.color.white));
        btg3.setBackgroundColor(getResources().getColor(R.color.white));
        btg4.setBackgroundColor(getResources().getColor(R.color.white));
        btg5.setBackgroundColor(getResources().getColor(R.color.white));
        btg6.setBackgroundColor(getResources().getColor(R.color.white));

        btg1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainGame.this, TictoeGame.class);
                startActivity(intent);
            }
        });
        btg2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainGame.this, russianroulette.class);
                startActivity(intent);
            }
        });
        btg3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainGame.this, slotmachine.class);
                startActivity(intent);
            }
        });
        btg4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainGame.this, reflex.class);
                startActivity(intent);
            }
        });
        btg5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainGame.this, evenodddice.class);
                startActivity(intent);
            }
        });
        btg6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainGame.this, roulette.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_exitintent,R.anim.anim_exit_exit);
    }
}
