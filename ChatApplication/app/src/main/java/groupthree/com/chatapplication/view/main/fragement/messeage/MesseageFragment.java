package groupthree.com.chatapplication.view.main.fragement.messeage;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import groupthree.com.chatapplication.R;
import groupthree.com.chatapplication.model.Message;
import groupthree.com.chatapplication.presenter.main.fragment.messagefragment.PresenterLogicMessage;
import groupthree.com.chatapplication.view.main.adapter.MessageAdapter;
import groupthree.com.chatapplication.view.searchfriend.SearchFriendActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class MesseageFragment extends Fragment implements ViewMessage{
    private EditText edtSearch;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<Message> arrMessage;
    private MessageAdapter messageAdapter;
    private PresenterLogicMessage presenterLogicMessage;
    public MesseageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_messeage, container, false);
        init(view);
        edtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iSearch = new Intent(view.getContext(), SearchFriendActivity.class);
                startActivity(iSearch);
                getActivity().overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
            }
        });
        return view;
    }



    private void init(View view) {
        edtSearch = view.findViewById(R.id.edtSearch);
        recyclerView = view.findViewById(R.id.rvMessenger);
        presenterLogicMessage = new PresenterLogicMessage(this);
        arrMessage = new ArrayList<>();

    }

    @Override
    public void loadData(ArrayList<Message> arrMess) {
        arrMessage.clear();
        if(arrMess!=null) {
            arrMessage.addAll(arrMess);
        }
        messageAdapter.notifyDataSetChanged();
    }

    @Override
    public void onStart() {
        super.onStart();
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        messageAdapter = new MessageAdapter(arrMessage,getContext(),presenterLogicMessage);
        recyclerView.setAdapter(messageAdapter);
        presenterLogicMessage.getDataMeaage();
    }
}
