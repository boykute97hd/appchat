package groupthree.com.chatapplication.view.setting;

import android.app.Dialog;
import android.app.Notification;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;


import com.google.firebase.auth.FirebaseAuth;

import groupthree.com.chatapplication.R;
import groupthree.com.chatapplication.view.ActionActivity;
import groupthree.com.chatapplication.view.signin.SignInActivity;

public class SettingActivity extends AppCompatActivity implements View.OnClickListener{
    private Dialog dialogProgress;
    private ImageButton imgbBack;
    private Button btnLogout, btnThongTin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        init();
        imgbBack.setOnClickListener(this);
        btnLogout.setOnClickListener(this);
        btnThongTin.setOnClickListener(this);

    }

    private void init() {
        imgbBack = findViewById(R.id.imgbBack);
        btnLogout =findViewById(R.id.btnSignout);
        btnThongTin =findViewById(R.id.btnThongTin);
        dialogProgress = new Dialog(SettingActivity.this);
    }


    private void signOut() {
        showDialogProgress();
        FirebaseAuth.getInstance().signOut();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(SettingActivity.this, ActionActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
                dialogProgress.dismiss();
            }
        },2000);

    }

    private void showDialogProgress() {
        dialogProgress.setContentView(R.layout.dialog_progress);
        dialogProgress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogProgress.setCanceledOnTouchOutside(false);
        dialogProgress.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_exitintent,R.anim.anim_exit_exit);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgbBack:
                onBackPressed();
                break;
            case R.id.btnThongTin:
                Intent i = new Intent(SettingActivity.this,InfomationActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
                break;
            case R.id.btnSignout:
                signOut();
                break;
        }
    }
}
