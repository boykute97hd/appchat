package groupthree.com.chatapplication.view.main.fragement.messeage;

import java.util.ArrayList;

import groupthree.com.chatapplication.model.Message;

public interface ViewMessage {
    void loadData(ArrayList<Message> arrMess);
}
