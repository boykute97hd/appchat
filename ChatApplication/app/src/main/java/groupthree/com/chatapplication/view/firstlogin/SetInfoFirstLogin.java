package groupthree.com.chatapplication.view.firstlogin;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import groupthree.com.chatapplication.R;
import groupthree.com.chatapplication.presenter.firstlogin.PresenterLogicFirstlogin;
import groupthree.com.chatapplication.view.main.MainActivity;
import groupthree.com.chatapplication.view.signin.SignInActivity;

public class SetInfoFirstLogin extends AppCompatActivity implements ViewFirshLogin{
    private PresenterLogicFirstlogin presenterLogicFirstlogin;
    private EditText edtDisplayName, edtBirthday;
    private Button btnSave;
    private Dialog dialogProgress;
    private ArrayAdapter<String> spinerAdapter;
    private ArrayList arrSpiner = new ArrayList();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_info_first_login);
        init();
        edtBirthday.setText("01/01/1990");
        edtBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePick();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogProgress();
                String genderString = "";
                RadioGroup gender= findViewById(R.id.rgGender);
                int id=gender.getCheckedRadioButtonId();
                if(id==-1) {
                    saveDataFail("Bạn phải chọn giới tính");
                    return;
                }
                RadioButton rad= findViewById(id);
                genderString = rad.getText().toString();
                Handler handler = new Handler();
                final String finalGenderString = genderString;
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        String displayNameTemp = edtDisplayName.getText().toString().trim();
                        String birthdayTemp = edtBirthday.getText().toString();
                        presenterLogicFirstlogin.saveInfoToData(displayNameTemp,birthdayTemp, finalGenderString);
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        dialogProgress.dismiss();
                    }
                },500);
            }
        });
    }

    private void showDialogProgress() {
        dialogProgress.setContentView(R.layout.dialog_progress);
        dialogProgress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogProgress.setCanceledOnTouchOutside(false);
        dialogProgress.show();
    }
    private void datePick() {
        String birthTemp = edtBirthday.getText().toString().trim();
        // Progress Date String
        String temp[] = birthTemp.split("/");
        int y = Integer.parseInt(temp[2]);
        int m = Integer.parseInt(temp[1]);
        int d = Integer.parseInt(temp[0]);
        final Calendar calendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(SetInfoFirstLogin.this,R.style.DateTimePicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(year,month,dayOfMonth);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                edtBirthday.setText(simpleDateFormat.format(calendar.getTime()));
            }
        },y,m-1,d);
        datePickerDialog.show();
    }

    private void init() {
        edtDisplayName = findViewById(R.id.edtDisplayName);
        edtBirthday = findViewById(R.id.edtBirthday);
       // edtGender = findViewById(R.id.edtGender);
        btnSave = findViewById(R.id.btnSetFirstLogin);
        dialogProgress = new Dialog(SetInfoFirstLogin.this);
        presenterLogicFirstlogin = new PresenterLogicFirstlogin(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        FirebaseAuth.getInstance().signOut();
        overridePendingTransition(R.anim.anim_exitintent,R.anim.anim_exit_exit);
    }

    @Override
    public void saveDataSucccess() {
        Toast.makeText(SetInfoFirstLogin.this,"Lưu thông tin thành công",Toast.LENGTH_SHORT).show();
        Intent iMainActivity = new Intent(SetInfoFirstLogin.this, MainActivity.class);
        iMainActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(iMainActivity);
        overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
    }

    @Override
    public void saveDataFail(String messeage) {
        Toast.makeText(SetInfoFirstLogin.this,messeage,Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        FirebaseAuth.getInstance().signOut();
    }
}
