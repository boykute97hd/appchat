package groupthree.com.chatapplication.model;

public class ImageModel {
    private String id;
    public String link;

    public ImageModel() {
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getLink() {
        return link;
    }
}
