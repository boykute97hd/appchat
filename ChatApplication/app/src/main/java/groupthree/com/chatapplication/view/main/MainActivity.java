package groupthree.com.chatapplication.view.main;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import groupthree.com.chatapplication.R;
import groupthree.com.chatapplication.model.Account;
import groupthree.com.chatapplication.presenter.main.PresenteLogicMain;
import groupthree.com.chatapplication.presenter.main.PresenterIpmMain;
import groupthree.com.chatapplication.view.active.ActiveActivity;
import groupthree.com.chatapplication.view.firstlogin.SetInfoFirstLogin;
import groupthree.com.chatapplication.view.main.adapter.SectionPagerAdapter;

public class MainActivity extends AppCompatActivity implements ViewMain{
    private ViewPager mViewPager;
    private SectionPagerAdapter sectionPagerAdapter;
    private TabLayout mTabLayout;
    private PresenteLogicMain presenteLogicMain;
    private ArrayList<Integer> arrIconNoSelect;
    private ArrayList<Integer> arrIconSelected;
    private ArrayList<String> arrTitle;
    private FirebaseUser firebaseUser;
    private DatabaseReference dataRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        setUpTab();
        int positon = mTabLayout.getSelectedTabPosition();
        mTabLayout.getTabAt(positon).setIcon(arrIconSelected.get(positon));
        mTabLayout.getTabAt(positon).setText(arrTitle.get(positon));
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                tab.setIcon(arrIconSelected.get(position));
                tab.setText(arrTitle.get(position));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                tab.setIcon(arrIconNoSelect.get(position));
                tab.setText("");
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }


    private void setUpTab() {
        for (int i= 0; i<sectionPagerAdapter.getCount();i++){
            mTabLayout.getTabAt(i).setIcon(arrIconNoSelect.get(i));
        }
    }

    private void init() {
        presenteLogicMain = new PresenteLogicMain(this);
        mViewPager = findViewById(R.id.viewPager);
        sectionPagerAdapter = new SectionPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(sectionPagerAdapter);
        mTabLayout = findViewById(R.id.tabLayout);
        mTabLayout.setupWithViewPager(mViewPager);
        arrIconNoSelect = new ArrayList<>();
        arrIconNoSelect.add(R.drawable.chat_noselect);
        arrIconNoSelect.add(R.drawable.contact_noselect);
        arrIconNoSelect.add(R.drawable.group_noselect);
        arrIconNoSelect.add(R.drawable.newfeed_noselect);
        arrIconNoSelect.add(R.drawable.other_noselect);
        arrIconSelected = new ArrayList<>();
        arrIconSelected.add(R.drawable.chat_selected);
        arrIconSelected.add(R.drawable.contact_selected);
        arrIconSelected.add(R.drawable.group_selected);
        arrIconSelected.add(R.drawable.newfeed_selected);
        arrIconSelected.add(R.drawable.other_selected);
        arrTitle = new ArrayList<>();
        arrTitle.add("Tin nhắn");
        arrTitle.add("Danh bạ");
        arrTitle.add("Nhóm");
        arrTitle.add("Nhật ký");
        arrTitle.add("Khác");
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        dataRef = FirebaseDatabase.getInstance().getReference().child("Accounts").child(firebaseUser.getUid());
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenteLogicMain.checkAccount();
        dataRef.child("online").setValue("Online");
    }

    @Override
    protected void onStop() {
        super.onStop();
        long time = System.currentTimeMillis();
        dataRef.child("online").onDisconnect().setValue(String.valueOf(time));
    }

    @Override
    public void intentAccount(int accountOption) {
        if(accountOption == 1){
            Intent i = new Intent(MainActivity.this, ActiveActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
            overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
        }
        if(accountOption == 2){
            Intent i = new Intent(MainActivity.this, SetInfoFirstLogin.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
            overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
        }
    }
}
