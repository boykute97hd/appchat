package groupthree.com.chatapplication.presenter.chat;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import groupthree.com.chatapplication.model.Account;
import groupthree.com.chatapplication.model.ImageModel;
import groupthree.com.chatapplication.model.MesseageModel;
import groupthree.com.chatapplication.view.chat.ViewChat;

public class PresenterLogicChat implements PresenterIpmChat {
    private ViewChat viewChat;

    public PresenterLogicChat(ViewChat viewChat) {
        this.viewChat = viewChat;
    }

    public ViewChat getViewChat() {
        return viewChat;
    }

    @Override
    public void getAccount(String uid) {
        DatabaseReference mAccount = FirebaseDatabase.getInstance().getReference().child("Accounts").child(uid);
        mAccount.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Account a = dataSnapshot.getValue(Account.class);
                getViewChat().loadInfo(a);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void getZoomChat(final String idTalk) {
        final String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference mData = FirebaseDatabase.getInstance().getReference();
        mData.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.child("Messages").child(uid).hasChild(idTalk)){
                    DataSnapshot snapshot = dataSnapshot.child("Messages").child(uid).child(idTalk).child("data");
                    ArrayList<MesseageModel> arrMessage = new ArrayList<>();
                    for (DataSnapshot temp : snapshot.getChildren()){
                        MesseageModel messeageModel = temp.getValue(MesseageModel.class);
                        messeageModel.setId(temp.getKey());
                        ArrayList<ImageModel> arrayList = new ArrayList();
                        if(temp.hasChild("image")){
                            DataSnapshot tempImage = temp.child("image");
                            for(DataSnapshot tempImage1: tempImage.getChildren()){
                                ImageModel imageModel = tempImage1.getValue(ImageModel.class);
                                imageModel.setId(tempImage1.getKey());
                                arrayList.add(imageModel);
                            }
                        }
                        messeageModel.setArrImage(arrayList);
                        arrMessage.add(messeageModel);
                    }
                    getViewChat().loadZoomChat(arrMessage);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void eventLoadPicture() {
        getViewChat().hideLayoutPicture();
    }

    @Override
    public void sendChat(String idTalk, ArrayList<Uri> arrAnh, String mess) {
        final String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference mData = FirebaseDatabase.getInstance().getReference().child("Messages");
        DatabaseReference mDataUser = mData.child(uid).child(idTalk);
        DatabaseReference mDataFriend = mData.child(idTalk).child(uid);
        final String id = mDataUser.push().getKey();
        mDataUser.child("data").child(id).child("from").setValue(uid);
        mDataUser.child("data").child(id).child("time").setValue(System.currentTimeMillis());
        mDataUser.child("data").child(id).child("mess").setValue(mess);
        mDataFriend.child("data").child(id).child("from").setValue(uid);
        mDataFriend.child("data").child(id).child("time").setValue(System.currentTimeMillis());
        mDataFriend.child("data").child(id).child("mess").setValue(mess);
        if(arrAnh.size()>0) {
            mDataUser.child("last").setValue("Bạn đã gửi "+arrAnh.size()+" hình ảnh");
            mDataFriend.child("last").setValue("Bạn nhận được "+arrAnh.size()+" hình ảnh");
            for (Uri uri : arrAnh) {
                final DatabaseReference chatUser =  mDataUser.child("data").child(id).child("image");
                final DatabaseReference chatFriend =  mDataFriend.child("data").child(id).child("image");
                final String idTemp = chatUser.push().getKey();
                final StorageReference storageUser = FirebaseStorage.getInstance().getReference().child("chat").child(idTemp);
                storageUser.putFile(uri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                        if(task.isSuccessful()){
                            storageUser.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    final String downloadUrl = uri.toString();
                                    chatUser.child(idTemp).child("link").setValue(downloadUrl);
                                    chatFriend.child(idTemp).child("link").setValue(downloadUrl);

                                }
                            });
                        }
                    }
                });
            }
        }else {
            mDataUser.child("last").setValue("Bạn: " +mess);
            mDataFriend.child("last").setValue(mess);
        }
        getZoomChat(idTalk);
    }

}
