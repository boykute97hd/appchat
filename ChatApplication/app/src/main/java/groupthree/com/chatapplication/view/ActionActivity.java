package groupthree.com.chatapplication.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import groupthree.com.chatapplication.R;
import groupthree.com.chatapplication.view.signin.SignInActivity;
import groupthree.com.chatapplication.view.signup.SignUpActivity;

public class ActionActivity extends AppCompatActivity {

    private Button btnSignIn;
    private Button btnSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action);
        btnSignIn = findViewById(R.id.btnSignInIntent);
        btnSignUp = findViewById(R.id.btnSignUpIntent);
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActionActivity.this, SignInActivity.class));
                overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
            }
        });
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActionActivity.this, SignUpActivity.class));
                overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
            }
        });
    }
}
