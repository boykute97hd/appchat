package groupthree.com.chatapplication.view.signin;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import groupthree.com.chatapplication.R;
import groupthree.com.chatapplication.presenter.signin.PresenterSignIn;
import groupthree.com.chatapplication.view.ActionActivity;
import groupthree.com.chatapplication.view.active.ActiveActivity;
import groupthree.com.chatapplication.view.firstlogin.SetInfoFirstLogin;
import groupthree.com.chatapplication.view.main.MainActivity;

public class SignInActivity extends AppCompatActivity implements ViewSignIn {
    private ImageButton imgbBack;
    private PresenterSignIn presenterSignIn;
    private EditText edtEmail, edtPassword;
    private Button btnLogin;
    private Dialog dialogProgress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        //method get control in layout
        init();

        // come back action
        imgbBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.anim_exitintent,R.anim.anim_exit_exit);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        String emailTemp = edtEmail.getText().toString();
                        String passwordTemp = edtPassword.getText().toString();
                        presenterSignIn.login(emailTemp,passwordTemp);
                    }
                },500);

            }
        });
    }
    private void init() {
        imgbBack = findViewById(R.id.imgbBack);
        btnLogin = findViewById(R.id.btnLogin);
        edtEmail = findViewById(R.id.edtEmailLogin);
        edtPassword = findViewById(R.id.edtPasswordLogin);
        dialogProgress= new Dialog(SignInActivity.this);

        presenterSignIn = new PresenterSignIn(this);
    }

    @Override
    public void showDialog() {
        dialogProgress.setContentView(R.layout.dialog_progress);
        dialogProgress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogProgress.setCanceledOnTouchOutside(false);
        dialogProgress.show();
    }

    @Override
    public void hideDialog() {
        dialogProgress.dismiss();
    }

    @Override
    public void loginSucccess() {
        Intent iMainActivity = new Intent(SignInActivity.this, MainActivity.class);
        iMainActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(iMainActivity);
        overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
    }

    @Override
    public void loginFail(int error) {
        if(error == 2){
            edtEmail.requestFocus();
            edtPassword.setText("");
            Toast.makeText(SignInActivity.this,"Bạn phải nhập đúng dạng Email!",Toast.LENGTH_SHORT).show();
        }
        if(error == 1){
            edtEmail.requestFocus();
            edtPassword.setText("");
            Toast.makeText(SignInActivity.this,"Bạn không được để trống bất kỳ trường nào!",Toast.LENGTH_SHORT).show();
        }
        if(error == 3){
            edtPassword.setText("");
            edtPassword.requestFocus();
            Toast.makeText(SignInActivity.this,"Mật khẩu nhập vào không chính xác!",Toast.LENGTH_SHORT).show();
        }
        if(error == 4){
            edtEmail.requestFocus();
            edtPassword.setText("");
            Toast.makeText(SignInActivity.this,"Email này chưa được sử dụng!",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_exitintent,R.anim.anim_exit_exit);
    }
}
