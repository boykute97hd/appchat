package groupthree.com.chatapplication.util.ContextMenu;

public interface IContextMenu {
    void onClickItem(int position);
}
