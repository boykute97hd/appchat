package groupthree.com.chatapplication.view.searchfriend;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.ArrayList;

import groupthree.com.chatapplication.R;
import groupthree.com.chatapplication.model.Account;
import groupthree.com.chatapplication.presenter.searchfriend.PresenterLogicSearchFriend;

public class SearchFriendActivity extends AppCompatActivity implements ViewSearch {
    private ImageButton imgbBack;
    private EditText edtSearch;
    private PresenterLogicSearchFriend presenterLogicSearchFriend;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private AdapterRecyclerViewFriend adapter;
    private ArrayList<Account> arrAccount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_friend);
        init();
        imgbBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        presenterLogicSearchFriend.listAccount("");
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                presenterLogicSearchFriend.listAccount(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void init() {
        imgbBack = findViewById(R.id.imgbBack);
        edtSearch = findViewById(R.id.edtSearch);
        presenterLogicSearchFriend = new PresenterLogicSearchFriend(this);
        recyclerView = findViewById(R.id.rcSearch);
        arrAccount = new ArrayList<>();
        layoutManager = new LinearLayoutManager(SearchFriendActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new AdapterRecyclerViewFriend(SearchFriendActivity.this,arrAccount,R.layout.layout_item_search);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_exitintent,R.anim.anim_exit_exit);
    }

    @Override
    public void getData(ArrayList<Account> arrList) {
        if(arrList.size()!=0) {
            arrAccount.clear();
            for (Account a : arrList) {
                arrAccount.add(a);
                adapter.notifyDataSetChanged();
            }
        }else {
            arrAccount.clear();
            adapter.notifyDataSetChanged();
        }
    }
}
