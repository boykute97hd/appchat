package groupthree.com.chatapplication.view.userprofile;

public interface ViewUserProfile {
    void loadAvatar(String link);
    void getDisplayName(String displayName);
    void getBirthday(String birthday);
    void getGender(String gender);
    void getAnhBia(String link);
}
