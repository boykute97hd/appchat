package groupthree.com.chatapplication.view.searchfriend;

import java.util.ArrayList;

import groupthree.com.chatapplication.model.Account;

public interface ViewSearch {
    void getData(ArrayList<Account> arrList);
}
