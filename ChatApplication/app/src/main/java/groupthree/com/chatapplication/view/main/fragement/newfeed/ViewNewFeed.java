package groupthree.com.chatapplication.view.main.fragement.newfeed;

import java.util.ArrayList;

import groupthree.com.chatapplication.model.StatusModel;

public interface ViewNewFeed {
    void loadData(ArrayList<StatusModel> status);
}
