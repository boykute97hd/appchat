package groupthree.com.chatapplication.model;

import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Account {
    private String id;
    public String active;
    public String avatar;
    public String birthday;
    public String displayName;
    public String firstLogin;
    public String gender;
    public String online;
    public String wallpaper;
    public String status;

    public Account() {
    }

    public Account(String id, String active, String avatar, String birthday, String displayName, String firstLogin, String gender, String online, String wallpaper, String status) {
        this.id = id;
        this.active = active;
        this.avatar = avatar;
        this.birthday = birthday;
        this.displayName = displayName;
        this.firstLogin = firstLogin;
        this.gender = gender;
        this.online = online;
        this.wallpaper = wallpaper;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
