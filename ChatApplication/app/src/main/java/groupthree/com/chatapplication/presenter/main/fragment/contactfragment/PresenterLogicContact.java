package groupthree.com.chatapplication.presenter.main.fragment.contactfragment;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import groupthree.com.chatapplication.model.Account;
import groupthree.com.chatapplication.model.ContactModel;
import groupthree.com.chatapplication.view.main.fragement.contact.ViewContact;

public class PresenterLogicContact implements PresenterIpmContact{
    private ViewContact viewContact;
    private FirebaseUser mUser;
    private DatabaseReference mData;

    public PresenterLogicContact(ViewContact viewContact) {
        this.viewContact = viewContact;
        mUser = FirebaseAuth.getInstance().getCurrentUser();
        mData = FirebaseDatabase.getInstance().getReference();
    }

    public ViewContact getViewContact() {
        return viewContact;
    }

    @Override
    public void getList() {
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                DataSnapshot mDataFriend = dataSnapshot.child("Friends").child(mUser.getUid());
                if(mDataFriend.exists()){
                    List<ContactModel> arrContact = new ArrayList<>();
                    for(DataSnapshot temp: mDataFriend.getChildren()){
                        ContactModel tempContact = new ContactModel();
                        tempContact.id = temp.getKey();
                        tempContact.date = Long.parseLong(temp.getValue().toString());
                        DataSnapshot dataAccount = dataSnapshot.child("Accounts").child(tempContact.id);
                        tempContact.account = dataAccount.getValue(Account.class);
                        arrContact.add(tempContact);
                    }
                    getViewContact().listFriend(arrContact);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        mData.addListenerForSingleValueEvent(valueEventListener);
    }

    @Override
    public void reload() {
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                DataSnapshot mDataFriend = dataSnapshot.child("Friends").child(mUser.getUid());
                if(mDataFriend.exists()){
                    List<ContactModel> arrContact = new ArrayList<>();
                    for(DataSnapshot temp: mDataFriend.getChildren()){
                        ContactModel tempContact = new ContactModel();
                        tempContact.id = temp.getKey();
                        tempContact.date = Long.parseLong(temp.getValue().toString());
                        DataSnapshot dataAccount = dataSnapshot.child("Accounts").child(tempContact.id);
                        tempContact.account = dataAccount.getValue(Account.class);
                        arrContact.add(tempContact);
                    }
                    getViewContact().listFriend(arrContact);
                    getViewContact().setRefresh(false);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        mData.addListenerForSingleValueEvent(valueEventListener);
    }
}
