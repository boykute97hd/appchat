package groupthree.com.chatapplication.util.ContextMenu;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import groupthree.com.chatapplication.R;

public class ContextMenu {
    private Context context;
    private ArrayList<String> arrMenu;
    private Dialog dialog;
    private TextView tvTitle;
    private ListView lvMenu;
    ArrayAdapter arrayAdapter;

    public ContextMenu(Context context, ArrayList<String> arrMenu) {
        this.context = context;
        this.arrMenu = arrMenu;
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.layout_contextmenu);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        tvTitle = dialog.findViewById(R.id.tvName);
        lvMenu = dialog.findViewById(R.id.lvMenu);
        arrayAdapter = new ArrayAdapter(context,android.R.layout.simple_list_item_1,arrMenu);
        lvMenu.setAdapter(arrayAdapter);
    }

    public void show(){
        dialog.show();
    }

    public void dismiss(){
        dialog.dismiss();
    }

    public void hide(){
        dialog.hide();
    }

    public void setName(String name){
        tvTitle.setText(name);
    }

    public void setOnClick(final IContextMenu iContextMenu){
        lvMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                iContextMenu.onClickItem(position);
            }
        });
    }

}
