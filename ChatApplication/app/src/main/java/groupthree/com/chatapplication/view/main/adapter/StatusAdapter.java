package groupthree.com.chatapplication.view.main.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import groupthree.com.chatapplication.R;
import groupthree.com.chatapplication.model.ImageModel;
import groupthree.com.chatapplication.model.Like;
import groupthree.com.chatapplication.model.StatusModel;
import groupthree.com.chatapplication.presenter.main.fragment.newfeedfragment.PresenterLogicNewFeed;

public class StatusAdapter extends RecyclerView.Adapter<StatusAdapter.ViewHolder> {
    private ArrayList<StatusModel> arrStatusModel;
    private Context context;
    private PresenterLogicNewFeed presenterLogicNewFeed;

    public StatusAdapter(ArrayList<StatusModel> arrStatusModel, Context context, PresenterLogicNewFeed presenterLogicNewFeed) {
        this.arrStatusModel = arrStatusModel;
        this.context = context;
        this.presenterLogicNewFeed = presenterLogicNewFeed;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_status,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final StatusModel statusModel = arrStatusModel.get(position);
        Log.d("status_id", "onBindViewHolder: "+statusModel.getId());
        holder.tvName.setText(statusModel.getAccount().displayName);
        holder.tvContent.setText(statusModel.getPost());
        if(statusModel.getAccount().avatar.equals("default")){
            holder.imgAnhDaiDien.setBackgroundResource(R.drawable.avt);
        }else {
            Picasso.get().load(statusModel.getAccount().avatar).into(holder.imgAnhDaiDien);
        }
        if(statusModel.getArrLike().size() !=0 || statusModel.getArrComment().size() != 0){
            holder.tvThongBao.setText(statusModel.getArrLike().size()+ " thích, "+statusModel.getArrComment()+" bình luận");
        }else {
            holder.tvThongBao.setText("Hãy là người đầu tiên yêu thích hoặc bình luận bài viết này");
        }
        if(statusModel.getArrImageModel().size() != 0){
            holder.setArrAnh(statusModel.getArrImageModel());
        }
        final String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        loadLike(holder.btnLike,isLike(statusModel));
        holder.btnLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isLike(statusModel)){
                    presenterLogicNewFeed.like(uid,statusModel.getId());
                    Drawable left = context.getResources().getDrawable(R.drawable.ic_like);
                    holder.btnLike.setCompoundDrawablesWithIntrinsicBounds(left,null,null,null);
                }else {
                    presenterLogicNewFeed.unLike(uid,statusModel.getId());
                    Drawable left = context.getResources().getDrawable(R.drawable.ic_liked);
                    holder.btnLike.setCompoundDrawablesWithIntrinsicBounds(left,null,null,null);
                }
            }
        });

    }
    private boolean isLike(StatusModel statusModel){
        boolean isLike = false;
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        for(Like like: statusModel.getArrLike()){
            if(like.getAccount().getId().equals(uid)){
                isLike = true;
            }
        }
        return isLike;
    }
    private void loadLike(Button btn,boolean like){
        if(like){
            Drawable left = context.getResources().getDrawable(R.drawable.ic_liked);
            btn.setCompoundDrawablesWithIntrinsicBounds(left,null,null,null);
        }else {
            Drawable left = context.getResources().getDrawable(R.drawable.ic_like);
            btn.setCompoundDrawablesWithIntrinsicBounds(left,null,null,null);
        }
    }
    @Override
    public int getItemCount() {
        return arrStatusModel.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        CircleImageView imgAnhDaiDien;
        TextView tvName, tvTime, tvContent, tvThongBao;
        ImageButton imgbContextMenu;
        RecyclerView rvAnh;
        Button btnLike, btnComment;
        View view;
        PictureStatusAdapter pictureStatusAdapter;
        RecyclerView.LayoutManager layoutManager;
        ArrayList<ImageModel> arrImageModel;
        public ViewHolder(View itemView) {
            super(itemView);
            imgAnhDaiDien = itemView.findViewById(R.id.imgAnhDaiDien);
            tvName = itemView.findViewById(R.id.tvDisplayName);
            tvTime = itemView.findViewById(R.id.tvTimeBaiViet);
            tvContent = itemView.findViewById(R.id.tvContent);
            tvThongBao = itemView.findViewById(R.id.tvThongBao);
            imgbContextMenu = itemView.findViewById(R.id.imgbContextMenu);
            rvAnh = itemView.findViewById(R.id.rvAnh);
            btnComment = itemView.findViewById(R.id.btnComment);
            btnLike = itemView.findViewById(R.id.btnLike);
            view = itemView;
            arrImageModel = new ArrayList<>();
            pictureStatusAdapter = new PictureStatusAdapter(arrImageModel,itemView.getContext());
            layoutManager =new LinearLayoutManager(itemView.getContext(),LinearLayoutManager.HORIZONTAL,false);
            rvAnh.setLayoutManager(layoutManager);
            rvAnh.setAdapter(pictureStatusAdapter);
        }
        public void setArrAnh(ArrayList<ImageModel> arrAnh){
            arrImageModel.clear();
            arrImageModel.addAll(arrAnh);
            pictureStatusAdapter.notifyDataSetChanged();
        }
    }
}
