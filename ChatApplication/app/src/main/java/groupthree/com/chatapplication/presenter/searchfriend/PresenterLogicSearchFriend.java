package groupthree.com.chatapplication.presenter.searchfriend;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import groupthree.com.chatapplication.model.Account;
import groupthree.com.chatapplication.view.searchfriend.ViewSearch;

public class PresenterLogicSearchFriend implements PresenterIpmSearchFriend {
    private ViewSearch viewSearch;
    private DatabaseReference mData;
    public ViewSearch getViewSearch() {
        return viewSearch;
    }

    public PresenterLogicSearchFriend(ViewSearch viewSearch) {
        this.viewSearch = viewSearch;
        mData = FirebaseDatabase.getInstance().getReference().child("Accounts");
    }


    @Override
    public void listAccount(final String keyword) {
        if(keyword.trim().length() != 0){
            ValueEventListener valueEventListener = new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        ArrayList<Account> arrAllAccount = new ArrayList<>();
                        for(DataSnapshot snapshot: dataSnapshot.getChildren()){
                            Account a = snapshot.getValue(Account.class);
                            a.setId(snapshot.getKey());
                            arrAllAccount.add(a);
                        }
                        ArrayList<Account> temp = new ArrayList<>();
                        for(Account t1:arrAllAccount){
                            if(t1.displayName.toLowerCase().contains(keyword.toLowerCase())){
                                temp.add(t1);
                            }
                        }
                        getViewSearch().getData(temp);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            };
            mData.addListenerForSingleValueEvent(valueEventListener);
        }else {
            ArrayList<Account> arrAllAccount = new ArrayList<>();
            getViewSearch().getData(arrAllAccount);
        }

    }
}
