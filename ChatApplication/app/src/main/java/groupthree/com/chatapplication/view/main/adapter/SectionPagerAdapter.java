package groupthree.com.chatapplication.view.main.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import groupthree.com.chatapplication.view.main.fragement.contact.ContactFragment;
import groupthree.com.chatapplication.view.main.fragement.group.GroupFragment;
import groupthree.com.chatapplication.view.main.fragement.messeage.MesseageFragment;
import groupthree.com.chatapplication.view.main.fragement.newfeed.NewfeddFrament;
import groupthree.com.chatapplication.view.main.fragement.other.OtherFragment;

public class SectionPagerAdapter extends FragmentPagerAdapter {
    public SectionPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                MesseageFragment messeageFragment = new MesseageFragment();
                return  messeageFragment;
            case 1:
                ContactFragment contactFragment = new ContactFragment();
                return contactFragment;
            case 2:
                GroupFragment groupFragment = new GroupFragment();
                return groupFragment;
            case 3:
                NewfeddFrament newFeedFragment = new NewfeddFrament();
                return newFeedFragment;
            case 4:
                OtherFragment otherFragment = new OtherFragment();
                return otherFragment;
            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return 5;
    }


}
