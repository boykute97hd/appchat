package groupthree.com.chatapplication.view.game;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import groupthree.com.chatapplication.R;

public class russianroulette extends AppCompatActivity {
    ImageView imageView;
    Button buttons, btback;
    TextView textView;

    List<String> bullets;
    boolean shuffed = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_russianroulette);
        mainApp();
    }

    public void mainApp() {
        try {
            imageView = findViewById(R.id.imageView);
            buttons = findViewById(R.id.buttons);
            btback = findViewById(R.id.btBack);
            textView = findViewById(R.id.textView);

//            buttons.setVisibility(View.INVISIBLE);

            bullets = new ArrayList<>();
            //Add bullet to the gun
            bullets.add("NO");
            bullets.add("NO");
            bullets.add("NO");
            bullets.add("NO");
            bullets.add("NO");
            bullets.add("YES");

            //Roll the barrel
            Collections.shuffle(bullets);
            btback.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            buttons.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Collections.shuffle(bullets);
                    imageView.setImageResource(R.drawable.gun);
                    shuffed = true;
                    textView.setText("Click on the image to shoot!");
                }
            });
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (shuffed) {
                        String currentBullets = bullets.get(0);
                        if (currentBullets.equals("YES")) {
                            imageView.setImageResource(R.drawable.bang);
                            textView.setText("Yow Are DEAD!");
                        } else {
                            imageView.setImageResource(R.drawable.click);
                            textView.setText("Yow Are Alive! Pass the gun to the next Player!");
                        }
                        shuffed = false;
                    } else {
                        textView.setText("First Roll in the Barrel!");
                    }
                }
            });
        } catch (Exception ex) {
            Toast.makeText(this, "Error in Main App!", Toast.LENGTH_LONG).show();
        }
    }
}
