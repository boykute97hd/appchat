package groupthree.com.chatapplication.view.main.fragement.contact;

import java.util.List;

import groupthree.com.chatapplication.model.ContactModel;

public interface ViewContact {
    void listFriend(List<ContactModel> arrFriend);
    void setRefresh(boolean enable);
}
