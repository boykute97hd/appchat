package groupthree.com.chatapplication.view.upstatus;

import groupthree.com.chatapplication.model.Account;

public interface ViewUpStatus {
    void loadView(Account account);
    void setEnableButtonPost(boolean enable);
    void finishActivity();
    void showDialogProgress();
    void hideDialogProgress();
}
