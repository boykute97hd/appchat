package groupthree.com.chatapplication.view.game;

import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.Random;

import groupthree.com.chatapplication.R;

public class slotmachine extends AppCompatActivity {
    Button btRoll, btBack;
    ImageView image1, image2, image3;
    Random r;
    RadioGroup rg_changestyle;
    RadioButton rb_classic, rb_blackandwhite;
    int img1, img2, img3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slotmachine);
        mainApp();
    }
    public void findcontrol() {
        btRoll = findViewById(R.id.btRoll);
        btBack = findViewById(R.id.btBack);

        rg_changestyle = findViewById(R.id.rg_changestyle);
        rb_classic = findViewById(R.id.rb_classic);
        rb_blackandwhite = findViewById(R.id.rb_blackandwhite);

        image1 = findViewById(R.id.image1);
        image2 = findViewById(R.id.image2);
        image3 = findViewById(R.id.image3);
    }

    public void mainApp() {
        r = new Random();

        findcontrol();
        btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btRoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rb_classic.isChecked()) {
                    //animate First image
                    image1.setImageResource(R.drawable.anim);
                    final AnimationDrawable image1anim = (AnimationDrawable) image1.getDrawable();
                    image1anim.start();
                    //animate Second image
                    image2.setImageResource(R.drawable.anim);
                    final AnimationDrawable image2anim = (AnimationDrawable) image2.getDrawable();
                    image2anim.start();
                    //animate Third image
                    image3.setImageResource(R.drawable.anim);
                    final AnimationDrawable image3anim = (AnimationDrawable) image3.getDrawable();
                    image3anim.start();

                    //Stop the animation
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            image1anim.stop();
                            image2anim.stop();
                            image3anim.stop();
                            setImages();
                            getScore();
                        }
                    }, 500);
                }
                if (rb_blackandwhite.isChecked()) {
                    //animate First image
                    image1.setImageResource(R.drawable.anim2);
                    final AnimationDrawable image1anim = (AnimationDrawable) image1.getDrawable();
                    image1anim.start();
                    //animate Second image
                    image2.setImageResource(R.drawable.anim2);
                    final AnimationDrawable image2anim = (AnimationDrawable) image2.getDrawable();
                    image2anim.start();
                    //animate Third image
                    image3.setImageResource(R.drawable.anim2);
                    final AnimationDrawable image3anim = (AnimationDrawable) image3.getDrawable();
                    image3anim.start();

                    //Stop the animation
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            image1anim.stop();
                            image2anim.stop();
                            image3anim.stop();
                            setImages2();
                            getScore();
                        }
                    }, 500);
                }
            }
        });
    }

    public void setImages() {
        //Random
        img1 = r.nextInt(7) + 1;
        img2 = r.nextInt(7) + 1;
        img3 = r.nextInt(7) + 1;

        //set first Images
        switch (img1) {
            case 1:
                image1.setImageResource(R.drawable.sm7);
                break;
            case 2:
                image1.setImageResource(R.drawable.smbanana);
                break;
            case 3:
                image1.setImageResource(R.drawable.smcherry);
                break;
            case 4:
                image1.setImageResource(R.drawable.smdiamond);
                break;
            case 5:
                image1.setImageResource(R.drawable.smgrape);
                break;
            case 6:
                image1.setImageResource(R.drawable.smorange);
                break;
            case 7:
                image1.setImageResource(R.drawable.smwatermellon);
                break;
        }
        //set Second Images
        switch (img2) {
            case 1:
                image2.setImageResource(R.drawable.sm7);
                break;
            case 2:
                image2.setImageResource(R.drawable.smbanana);
                break;
            case 3:
                image2.setImageResource(R.drawable.smcherry);
                break;
            case 4:
                image2.setImageResource(R.drawable.smdiamond);
                break;
            case 5:
                image2.setImageResource(R.drawable.smgrape);
                break;
            case 6:
                image2.setImageResource(R.drawable.smorange);
                break;
            case 7:
                image2.setImageResource(R.drawable.smwatermellon);
                break;
        }
        //set Third Images
        switch (img3) {
            case 1:
                image3.setImageResource(R.drawable.sm7);
                break;
            case 2:
                image3.setImageResource(R.drawable.smbanana);
                break;
            case 3:
                image3.setImageResource(R.drawable.smcherry);
                break;
            case 4:
                image3.setImageResource(R.drawable.smdiamond);
                break;
            case 5:
                image3.setImageResource(R.drawable.smgrape);
                break;
            case 6:
                image3.setImageResource(R.drawable.smorange);
                break;
            case 7:
                image3.setImageResource(R.drawable.smwatermellon);
                break;
        }
    }

    public void setImages2() {
        //Random
        img1 = r.nextInt(7) + 1;
        img2 = r.nextInt(7) + 1;
        img3 = r.nextInt(7) + 1;

        //set first Images
        switch (img1) {
            case 1:
                image1.setImageResource(R.drawable.sm7bw);
                break;
            case 2:
                image1.setImageResource(R.drawable.smbananabw);
                break;
            case 3:
                image1.setImageResource(R.drawable.smcherrybw);
                break;
            case 4:
                image1.setImageResource(R.drawable.smdiamondbw);
                break;
            case 5:
                image1.setImageResource(R.drawable.smgrapebw);
                break;
            case 6:
                image1.setImageResource(R.drawable.smorangebw);
                break;
            case 7:
                image1.setImageResource(R.drawable.smwatermellonbw);
                break;
        }
        //set Second Images
        switch (img2) {
            case 1:
                image2.setImageResource(R.drawable.sm7bw);
                break;
            case 2:
                image2.setImageResource(R.drawable.smbananabw);
                break;
            case 3:
                image2.setImageResource(R.drawable.smcherrybw);
                break;
            case 4:
                image2.setImageResource(R.drawable.smdiamondbw);
                break;
            case 5:
                image2.setImageResource(R.drawable.smgrapebw);
                break;
            case 6:
                image2.setImageResource(R.drawable.smorangebw);
                break;
            case 7:
                image2.setImageResource(R.drawable.smwatermellonbw);
                break;
        }
        //set Third Images
        switch (img3) {
            case 1:
                image3.setImageResource(R.drawable.sm7bw);
                break;
            case 2:
                image3.setImageResource(R.drawable.smbananabw);
                break;
            case 3:
                image3.setImageResource(R.drawable.smcherrybw);
                break;
            case 4:
                image3.setImageResource(R.drawable.smdiamondbw);
                break;
            case 5:
                image3.setImageResource(R.drawable.smgrapebw);
                break;
            case 6:
                image3.setImageResource(R.drawable.smorangebw);
                break;
            case 7:
                image3.setImageResource(R.drawable.smwatermellonbw);
                break;
        }
    }

    public void getScore() {
        //3 of the same Img
        if (img1 == img2 && img2 == img3) {
            Toast.makeText(this, "JACKPOT!!!", Toast.LENGTH_LONG).show();
        }
        //2 of the same img
        if (img1 == img2 || img2 == img3 || img3 == img1) {
            Toast.makeText(this, "AWE SOME!!!", Toast.LENGTH_LONG).show();
        }
    }
}
