package groupthree.com.chatapplication.presenter.firstlogin;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

import groupthree.com.chatapplication.view.firstlogin.ViewFirshLogin;

public class PresenterLogicFirstlogin implements PresenterIpmFirstLogin {
    private ViewFirshLogin viewFirshLogin;
    private DatabaseReference mDatabase;
    private FirebaseUser mUser;

    public PresenterLogicFirstlogin(ViewFirshLogin viewFirshLogin) {
        this.viewFirshLogin = viewFirshLogin;
    }

    public ViewFirshLogin getViewFirshLogin() {
        return viewFirshLogin;
    }

    public void setViewFirshLogin(ViewFirshLogin viewFirshLogin) {
        this.viewFirshLogin = viewFirshLogin;
    }

    @Override
    public void saveInfoToData(final String displayName, String birthday, String gender){
        if(TextUtils.isEmpty(displayName)){
            getViewFirshLogin().saveDataFail("Bạn phải nhập tên hiển thị");
        }else {
            mUser = FirebaseAuth.getInstance().getCurrentUser();
            String uid = mUser.getUid();
            mDatabase = FirebaseDatabase.getInstance().getReference().child("Accounts").child(uid);
            HashMap<String,Object> map = new HashMap<>();
            map.put("displayName",displayName);
            map.put("birthday",birthday);
            map.put("gender",gender);
            map.put("firstLogin","false");
            map.put("wallpaper","default");
            map.put("avatar","default");
            mDatabase.updateChildren(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        getViewFirshLogin().saveDataSucccess();
                    }else {
                        getViewFirshLogin().saveDataFail("Có lỗi trong quá trình lưu thông tin");
                    }
                }
            });
        }
    }
}
