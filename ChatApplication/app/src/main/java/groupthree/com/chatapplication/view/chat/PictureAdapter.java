package groupthree.com.chatapplication.view.chat;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.util.ArrayList;

import groupthree.com.chatapplication.R;
import groupthree.com.chatapplication.presenter.chat.PresenterLogicChat;

public class PictureAdapter extends RecyclerView.Adapter<PictureAdapter.ViewHolder> {
    private ArrayList<Uri> arrUri;
    private int resource;
    private PresenterLogicChat presenterLogicChat;

    public PictureAdapter(ArrayList<Uri> arrUri, int resource,PresenterLogicChat presenterLogicChat) {
        this.arrUri = arrUri;
        this.resource = resource;
        this.presenterLogicChat = presenterLogicChat;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resource,parent,false);
        PictureAdapter.ViewHolder viewHolder = new PictureAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Uri uri = arrUri.get(position);
        holder.img.setImageURI(uri);
        holder.img.setScaleType(ImageView.ScaleType.FIT_CENTER);
        holder.btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrUri.remove(uri);
                notifyDataSetChanged();
                if(arrUri.size()==0){
                    presenterLogicChat.eventLoadPicture();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrUri.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView img;
        ImageButton btnDel;

        public ViewHolder(View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.imgPictureSelect);
            btnDel = itemView.findViewById(R.id.btnDelete);
        }
    }
}
