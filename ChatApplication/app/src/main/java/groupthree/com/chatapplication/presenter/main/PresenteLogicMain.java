package groupthree.com.chatapplication.presenter.main;

import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import groupthree.com.chatapplication.view.main.ViewMain;

public class PresenteLogicMain implements PresenterIpmMain {
    private ViewMain viewMain;
    public PresenteLogicMain(ViewMain viewMain) {
        this.viewMain = viewMain;
    }

    public ViewMain getViewMain() {
        return viewMain;
    }

    @Override
    public void checkAccount() {
        FirebaseUser mUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference mDb = FirebaseDatabase.getInstance().getReference().child("Accounts").child(mUser.getUid());
        mDb.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String active = dataSnapshot.child("active").getValue().toString();
                String firstLogin = dataSnapshot.child("firstLogin").getValue().toString();
                if(active.equals("false")){
                    getViewMain().intentAccount(1);
                }else {
                    if(firstLogin.equals("true")){
                        getViewMain().intentAccount(2);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
