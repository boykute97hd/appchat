package groupthree.com.chatapplication.view.active;

public interface ViewActive {
    void actived();
    void notActived();
    void resendSucccess();
    void resendFail();
}
