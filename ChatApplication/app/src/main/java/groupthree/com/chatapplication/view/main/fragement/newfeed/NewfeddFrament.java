package groupthree.com.chatapplication.view.main.fragement.newfeed;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

import groupthree.com.chatapplication.R;
import groupthree.com.chatapplication.model.StatusModel;
import groupthree.com.chatapplication.presenter.main.fragment.newfeedfragment.PresenterLogicNewFeed;
import groupthree.com.chatapplication.view.main.adapter.StatusAdapter;
import groupthree.com.chatapplication.view.searchfriend.SearchFriendActivity;
import groupthree.com.chatapplication.view.upstatus.UpStatusActivity;

public class NewfeddFrament extends Fragment implements ViewNewFeed{
    private EditText edtSearch,edtStatus;
    private Button btnPostPicture, btnPostVideo;
    private PresenterLogicNewFeed presenterLogicNewFeed;
    private ArrayList<StatusModel> arrStatus;
    private RecyclerView.LayoutManager layoutManager;
    private StatusAdapter statusAdapter ;
    private RecyclerView rvStatus;
    private int total = 20;
    public NewfeddFrament() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_newfedd_frament, container, false);
        init(view);
        edtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iSearch = new Intent(view.getContext(), SearchFriendActivity.class);
                startActivity(iSearch);
                getActivity().overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
            }
        });
        edtStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(view.getContext(), UpStatusActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
            }
        });
        btnPostPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(view.getContext(), UpStatusActivity.class);
                intent.putExtra("picture",1);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
            }
        });
        return view;

    }

    private void init(View view) {
        edtSearch = view.findViewById(R.id.edtSearch);
        edtStatus = view.findViewById(R.id.edtTrangThai);
        btnPostPicture = view.findViewById(R.id.btnPostPicture);
        btnPostVideo = view.findViewById(R.id.btnPostVideo);
        presenterLogicNewFeed = new PresenterLogicNewFeed(this);
        arrStatus = new ArrayList<>();
        statusAdapter = new StatusAdapter(arrStatus,getContext(),presenterLogicNewFeed);
        layoutManager = new LinearLayoutManager(getContext());
        rvStatus = view.findViewById(R.id.rvStatus);
        rvStatus.setLayoutManager(layoutManager);
        rvStatus.setAdapter(statusAdapter);

    }

    @Override
    public void onStart() {
        super.onStart();
        presenterLogicNewFeed.loadData(total);
    }

    @Override
    public void loadData(ArrayList<StatusModel> status) {
        arrStatus.clear();
        if(status.size()!=0 && status!=null){
            arrStatus.addAll(status);
        }
        statusAdapter.notifyDataSetChanged();
    }
}
