package groupthree.com.chatapplication.view.active;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import groupthree.com.chatapplication.R;
import groupthree.com.chatapplication.presenter.active.PresenterLogicActive;
import groupthree.com.chatapplication.view.firstlogin.SetInfoFirstLogin;

public class ActiveActivity extends AppCompatActivity implements ViewActive {
    private String check;
    private PresenterLogicActive presenterLogicActive;
    private Button btnCountiue, btnReendCode;
    private ImageButton imgbBack;
    Thread t;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active);
        init();
        notActived();
        t = new Thread(){
            @Override
            public void run() {
            while (!isInterrupted()){
                try {
                    Thread.sleep(1000);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            check = presenterLogicActive.checkActiveAccount();
                            if(check.equals("true")){
                                presenterLogicActive.enabaleButtonContinue();
                            }
                        }
                    });
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            }
        };
        imgbBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                finish();
                overridePendingTransition(R.anim.anim_exitintent,R.anim.anim_exit_exit);
            }
        });

        t.start();
        btnReendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenterLogicActive.resendCodeVerify();
            }
        });
        btnCountiue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iFirstLogin = new Intent(ActiveActivity.this, SetInfoFirstLogin.class);
                startActivity(iFirstLogin);
                overridePendingTransition(R.anim.anim_intent,R.anim.anim_exit);
                finish();
            }
        });
    }

    private void init() {
        presenterLogicActive = new PresenterLogicActive(this);
        btnCountiue = findViewById(R.id.btnActive);
        btnReendCode = findViewById(R.id.btnResendCodeVerify);
        imgbBack = findViewById(R.id.imgbBack);
    }

    @Override
    public void actived() {
        btnCountiue.setVisibility(View.VISIBLE);
    }

    @Override
    public void notActived() {
        btnCountiue.setVisibility(View.GONE);
    }

    @Override
    public void resendSucccess() {
        Toast.makeText(ActiveActivity.this,"Gửi lại thành công",Toast.LENGTH_SHORT).show();
    }
    @Override
    public void resendFail() {
        Toast.makeText(ActiveActivity.this,"Gửi lại thất bại",Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        FirebaseAuth.getInstance().signOut();
        overridePendingTransition(R.anim.anim_exitintent,R.anim.anim_exit_exit);
    }

    @Override
    protected void onStop() {
        super.onStop();
        FirebaseAuth.getInstance().signOut();
    }
}
