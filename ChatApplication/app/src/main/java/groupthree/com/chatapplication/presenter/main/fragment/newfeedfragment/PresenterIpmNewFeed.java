package groupthree.com.chatapplication.presenter.main.fragment.newfeedfragment;

public interface PresenterIpmNewFeed {
    void loadData(int total);
    void like(String uid,String idPost);
    void unLike(String uid,String idPost);
}
