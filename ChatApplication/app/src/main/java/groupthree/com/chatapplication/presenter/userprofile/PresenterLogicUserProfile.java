package groupthree.com.chatapplication.presenter.userprofile;

import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import groupthree.com.chatapplication.model.Account;
import groupthree.com.chatapplication.view.userprofile.ViewUserProfile;

public class PresenterLogicUserProfile implements PresenterIpmUserProfile {
    private ViewUserProfile viewUserProfile;
    private FirebaseUser mUser;
    private DatabaseReference mDataUser;
    Account account;
    public PresenterLogicUserProfile(ViewUserProfile viewUserProfile) {
        this.viewUserProfile = viewUserProfile;
    }

    public ViewUserProfile getViewUserProfile() {
        return viewUserProfile;
    }

    public void setViewUserProfile(ViewUserProfile viewUserProfile) {
        this.viewUserProfile = viewUserProfile;
    }

    @Override
    public void loadUserProfile() {
        mUser = FirebaseAuth.getInstance().getCurrentUser();
        mDataUser = FirebaseDatabase.getInstance().getReference().child("Accounts").child(mUser.getUid());
        mDataUser.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                account = dataSnapshot.getValue(Account.class);
                getViewUserProfile().getAnhBia(account.wallpaper);
                getViewUserProfile().getBirthday(account.birthday);
                getViewUserProfile().getDisplayName(account.displayName);
                getViewUserProfile().getGender(account.gender);
                getViewUserProfile().loadAvatar(account.avatar);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void loadUserTimeline() {

    }
}
